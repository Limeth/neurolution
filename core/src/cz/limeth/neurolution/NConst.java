package cz.limeth.neurolution;

import java.util.Random;

public class NConst
{
	public static Random RANDOM = new Random();
	public static final float MIN_VIEW_WIDTH = 400, MIN_VIEW_HEIGHT = 800,
			MAIN_MENU_BUTTON_WIDTH = 256, MAIN_MENU_BUTTON_HEIGHT = 48, MAIN_MENU_BUTTON_SPACING = 16,
			CREATURE_MAX_ENERGY_MODIFIER = 20000;
	public static final String PATH_SAVES = "saves/";
}
