package cz.limeth.neurolution.entities;

import java.util.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;

public class ComponentWall implements SComponent
{
	private static final Vector2 $vertex1 = new Vector2(), $vertex2 = new Vector2();
	private static final Color $color = new Color();
	private final SEntity entity;
	private final Fixture fixture;
	private Color color;
	
	public ComponentWall(SEntity entity, Fixture fixture, Color color)
	{
		Objects.requireNonNull(color, "The color must not be null!");
		
		this.entity = entity;
		this.fixture = fixture;
		this.color = color;
	}
	
	@Override
	public SEntity getEntity()
	{
		return entity;
	}
	
	@Override
	public Fixture getFixture()
	{
		return fixture;
	}
	
	@Override
	public void getColor(Color color)
	{
		color.set(this.color);
	}
	
	public void setColor(Color color)
	{
		Objects.requireNonNull(color, "The color must not be null!");
		
		this.color = color;
	}
	
	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		Body body = entity.getBody();
		Vector2 position = body.getPosition();
		getColor($color);
		
		renderer.setColor($color);
		
		for(Fixture fixture : body.getFixtureList())
		{
			EdgeShape shape = (EdgeShape) fixture.getShape();
			
			shape.getVertex1($vertex1);
			shape.getVertex2($vertex2);
			$vertex1.add(position);
			$vertex2.add(position);
			renderer.rectLine($vertex1, $vertex2, 1f / 8);
		}
	}
	
	@Override
	public void dispose()
	{
		
	}
}
