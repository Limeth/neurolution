package cz.limeth.neurolution.entities;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Disposable;

public interface SEntity extends SRenderable, Disposable
{
	public Body getBody();
	public void step(float delta);
	public SComponent[] getComponents();
	public SComponent getComponent(Fixture fixture);
}
