package cz.limeth.neurolution.entities;

import java.util.HashSet;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Disposable;

public class SEntitySet extends HashSet<SEntity> implements Disposable
{
	private static final long serialVersionUID = 7520816432859059586L;
	
	public void stepAll(float delta)
	{
		for(SEntity entity : this)
			entity.step(delta);
	}
	
	public void renderAll(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		for(SEntity entity : this)
			entity.render(renderer, polygonSpriteBatch, matrix, delta);
	}
	
	public SEntity get(Body body)
	{
		for(SEntity entity : this)
			if(entity.getBody() == body)
				return entity;
		
		return null;
	}
	
	@Override
	public void dispose()
	{
		for(SEntity entity : this)
			entity.dispose();
	}
	
	@Override
	public SEntitySet clone()
	{
		SEntitySet set = new SEntitySet();
		
		set.addAll(this);
		
		return set;
	}
}
