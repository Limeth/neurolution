package cz.limeth.neurolution.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import cz.limeth.neurolution.throwables.SpawnException;

public interface SSpawnable
{
	public SSpawnable spawn(World world, Vector2 position) throws SpawnException;
	public void despawn() throws SpawnException;
	
	/**
	 * @return The time in millis or null if not spawned
	 */
	public Long getSpawnedAt();
	public boolean isSpawned();
}
