package cz.limeth.neurolution.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import cz.limeth.neurolution.util.BodyBuilder;
import cz.limeth.neurolution.util.BodyDefBuilder;
import cz.limeth.neurolution.util.FixtureDefBuilder;

public class EntityWall implements SEntity
{
	private final Body body;
	private final ComponentWall[] components = new ComponentWall[4];
	
	public EntityWall(World world, Vector2 position, Vector2 size)
	{
		float top = size.y / 2;
		float bottom = -size.y / 2;
		float right = size.x / 2;
		float left = -size.x / 2;
		EdgeShape topWall = new EdgeShape();
		EdgeShape bottomWall = new EdgeShape();
		EdgeShape rightWall = new EdgeShape();
		EdgeShape leftWall = new EdgeShape();
		
		topWall.set(left, top, right, top);
		bottomWall.set(left, bottom, right, bottom);
		rightWall.set(right, bottom, right, top);
		leftWall.set(left, bottom, left, top);
		
		body = new BodyBuilder().bodyDef(
				new BodyDefBuilder().position(position).build()
			).world(world).build();
		
		components[0] = new ComponentWall(this, body.createFixture(new FixtureDefBuilder().shape(topWall).build()), Color.BLUE);
		components[1] = new ComponentWall(this, body.createFixture(new FixtureDefBuilder().shape(bottomWall).build()), Color.BLUE);
		components[2] = new ComponentWall(this, body.createFixture(new FixtureDefBuilder().shape(rightWall).build()), Color.BLUE);
		components[3] = new ComponentWall(this, body.createFixture(new FixtureDefBuilder().shape(leftWall).build()), Color.BLUE);
		
		topWall.dispose();
		bottomWall.dispose();
		rightWall.dispose();
		leftWall.dispose();
	}
	
	@Override
	public Body getBody()
	{
		return body;
	}
	
	@Override
	public void step(float delta)
	{
	}

	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		for(SComponent component : components)
			component.render(renderer, polygonSpriteBatch, matrix, delta);
	}

	@Override
	public ComponentWall[] getComponents()
	{
		ComponentWall[] components = new ComponentWall[this.components.length];
		
		System.arraycopy(this.components, 0, components, 0, this.components.length);
		
		return components;
	}

	@Override
	public SComponent getComponent(Fixture fixture)
	{
		for(SComponent component : components)
			if(component.getFixture() == fixture)
				return component;
		
		return null;
	}
	
	@Override
	public void dispose()
	{
		for(SComponent component : components)
			component.dispose();
	}
}
