package cz.limeth.neurolution.entities;

import java.util.function.Consumer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import cz.limeth.neurolution.screen.Simulation;
import cz.limeth.neurolution.throwables.SpawnException;
import cz.limeth.neurolution.util.BodyDefBuilder;

public class EntityFood implements SEntity, SSpawnable
{
	private final int duration;
	private int step;
	private Body body;
	private ComponentFood componentFood;
	private float size;
	private Long spawnedAt;
	private Consumer<EntityFood> onDespawn;
	
	public EntityFood(float size, int duration, Consumer<EntityFood> onDespawn)
	{
		this.size = size;
		this.duration = duration;
		this.onDespawn = onDespawn;
	}
	
	@Override
	public EntityFood spawn(World world, Vector2 position)
	{
		if(isSpawned())
			throw new SpawnException(this, this + " has been already spawned!");
		
		body = world.createBody(new BodyDefBuilder().position(position).type(BodyType.DynamicBody).linearDamping(0.1f).build());
		componentFood = new ComponentFood(this);
		spawnedAt = TimeUtils.millis();
		
		return this;
	}
	
	@Override
	public void despawn()
	{
		if(!isSpawned())
			throw new SpawnException(this, this + " has not been spawned yet!");
		
		if(onDespawn != null)
			onDespawn.accept(this);
		
		World world = body.getWorld();
		world.destroyBody(body);
		
		body = null;
		componentFood = null;
		spawnedAt = null;
	}
	
	@Override
	public Long getSpawnedAt()
	{
		return spawnedAt;
	}
	
	@Override
	public boolean isSpawned()
	{
		return spawnedAt != null;
	}
	
	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		if(!isSpawned())
			return;
		
		componentFood.render(renderer, polygonSpriteBatch, matrix, delta);
	}

	@Override
	public void dispose()
	{
		if(isSpawned())
			despawn();
	}

	@Override
	public Body getBody()
	{
		return body;
	}

	@Override
	public void step(float delta)
	{
		step++;
		
		if(isSpawned() && isDead())
			postDespawn();
		
		if(isSpawned() && !isDead() && !isEaten())
			componentFood.updateShape();
	}
	
	public void postDespawn()
	{
		Simulation.getInstance().runLater(new Runnable() {
			@Override
			public void run()
			{
				if(isSpawned())
					despawn();
			}
		});
	}
	
	public void getColor(Color color)
	{
		if(isDead())
		{
			color.set(Color.DARK_GRAY);
			return;
		}
		
		float health = getHealth();
		color.r = 0.5f - health * 0.5f;
		color.g = health * 0.5f + 0.5f;
		color.b = 0;
	}
	
	public float getRadius()
	{
		return (float) Math.sqrt(size / Math.PI);
	}
	
	public float getSize()
	{
		return size;
	}
	
	public void setSize(float size)
	{
		this.size = size;
	}
	
	public float decrease(float amount)
	{
		return this.size -= amount;
	}
	
	public float getHealth()
	{
		return 1 - (float) step / (float) duration;
	}
	
	public boolean isEaten()
	{
		return size <= 0;
	}
	
	public boolean isDead()
	{
		return getHealth() <= 0;
	}
	
	@Override
	public SComponent[] getComponents()
	{
		return new SComponent[] { componentFood };
	}

	@Override
	public SComponent getComponent(Fixture fixture)
	{
		if(fixture == componentFood.getFixture())
			return componentFood;
		
		return null;
	}
	
	public Consumer<EntityFood> getOnDespawn()
	{
		return onDespawn;
	}
	
	public void setOnDespawn(Consumer<EntityFood> onDespawn)
	{
		this.onDespawn = onDespawn;
	}
}
