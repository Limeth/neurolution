package cz.limeth.neurolution.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;

import cz.limeth.neurolution.util.FixtureDefBuilder;

public class ComponentFood implements SComponent
{
	private static final Color $color = new Color();
	private final EntityFood entity;
	private final Fixture fixture;
	
	public ComponentFood(EntityFood entity)
	{
		this.entity = entity;
		Body body = entity.getBody();
		CircleShape shape = new CircleShape();
		float radius = entity.getRadius();
		shape.setRadius(radius);
		fixture = body.createFixture(new FixtureDefBuilder().shape(shape).friction(0.8f).restitution(0.2f).density(1000).build());
		
		shape.dispose();
	}
	
	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		Body body = entity.getBody();
		Vector2 position = body.getPosition();
		float radius = entity.getRadius();
		getColor($color);
		
		renderer.setColor($color);
		renderer.circle(position.x, position.y, radius);
	}
	
	public void updateShape()
	{
		CircleShape shape = (CircleShape) fixture.getShape();
		float radius = entity.getRadius();
		
		shape.setRadius(radius);
	}
	
	@Override
	public void dispose()
	{
		
	}

	@Override
	public Fixture getFixture()
	{
		return fixture;
	}

	@Override
	public SEntity getEntity()
	{
		return entity;
	}

	@Override
	public void getColor(Color color)
	{
		entity.getColor(color);
	}
}
