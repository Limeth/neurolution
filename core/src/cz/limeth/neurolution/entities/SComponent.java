package cz.limeth.neurolution.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Disposable;

public interface SComponent extends SRenderable, Disposable
{
	public Fixture getFixture();
	public SEntity getEntity();
	public void getColor(Color color);
}
