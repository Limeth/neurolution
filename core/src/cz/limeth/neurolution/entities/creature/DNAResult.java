package cz.limeth.neurolution.entities.creature;

import com.google.gson.annotations.Expose;

public class DNAResult
{
	@Expose private DNA dna;
	@Expose private int lifetime;
	
	public DNAResult(DNA dna, int lifetime)
	{
		this.dna = dna;
		this.lifetime = lifetime;
	}

	public DNA getDNA()
	{
		return dna;
	}
	
	public void setDNA(DNA dna)
	{
		this.dna = dna;
	}
	
	public void setLifetime(int lifetime)
	{
		this.lifetime = lifetime;
	}
	
	public int getLifetime()
	{
		return lifetime;
	}
	
	public int getLifetimeSeconds()
	{
		return lifetime / 60;
	}
}
