package cz.limeth.neurolution.entities.creature;

import com.google.gson.annotations.Expose;

public class EyeDefinition implements Cloneable
{
	@Expose private float width, viewDistance;
	@Expose private ColorTranslationAbility colorTranslationAbility;
	
	public EyeDefinition(float width, float viewDistance, ColorTranslationAbility colorTranslationAbility)
	{
		this.width = width;
		this.viewDistance = viewDistance;
		this.colorTranslationAbility = colorTranslationAbility;
	}
	
	public float getRequiredAngle(float bodyRadius)
	{
		return (float) Math.atan2(width / 2, bodyRadius) * 2;
	}
	
	public float getViewDistance()
	{
		return viewDistance;
	}
	
	public void setViewDistance(float viewDistance)
	{
		this.viewDistance = viewDistance;
	}
	
	public float getWidth()
	{
		return width;
	}
	
	public void setWidth(float width)
	{
		this.width = width;
	}
	
	public ColorTranslationAbility getColorTranslationAbility()
	{
		return colorTranslationAbility;
	}
	
	public void setColorTranslationAbility(ColorTranslationAbility colorTranslationAbility)
	{
		this.colorTranslationAbility = colorTranslationAbility;
	}
	
	public EyeDefinition clone()
	{
		return new EyeDefinition(width, viewDistance, colorTranslationAbility);
	}
}
