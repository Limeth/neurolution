package cz.limeth.neurolution.entities.creature;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;

import cz.limeth.neurolution.entities.SComponent;
import cz.limeth.neurolution.screen.Simulation;
import cz.limeth.neurolution.util.FixtureDefBuilder;

public class ComponentBody implements SComponent
{
	private static final Color $color = new Color();
	private final EntityCreature entity;
	private final Fixture fixture;
	
	public ComponentBody(EntityCreature entity)
	{
		CircleShape shape = new CircleShape();
		float radius = entity.getDNA().getRadius();
		
		shape.setRadius(radius);
		
		Body body = entity.getBody();
		this.entity = entity;
		this.fixture = body.createFixture(new FixtureDefBuilder()
			.density(100).restitution(0.75f).friction(0.25f).shape(shape).build());
		
		shape.dispose();
	}

	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		Body body = entity.getBody();
		Vector2 position = body.getPosition();
		Vector2 direction = entity.getDirection();
		CircleShape circleShape = (CircleShape) fixture.getShape();
		float radius = circleShape.getRadius();
		getColor($color);
		
		if(Simulation.getInstance().isSelected(entity))
		{
			float inset = 0.02f;
			
			renderer.setColor(Color.WHITE);
			renderer.circle(position.x, position.y, radius + 0.05f, 16);
			
			if(!entity.isDead())
			{
				renderer.setColor(Color.GRAY);
				renderer.rect(position.x + radius + 0.1f, position.y - radius, 0.2f, radius * 2);
				
				renderer.setColor(Color.DARK_GRAY);
				renderer.rect(position.x + radius + 0.1f + inset, position.y - radius + inset, 0.2f - inset * 2, radius * 2 - inset * 2);
				
				renderer.setColor(Color.WHITE);
				renderer.rect(position.x + radius + 0.1f + inset, position.y - radius + inset, 0.2f - inset * 2, radius * 2 * entity.getEnergyPercentage() - inset * 2);
			}
		}
		
		renderer.setColor($color);
		renderer.circle(position.x, position.y, radius, 16);
		renderer.setColor(getLineColor());
		renderer.rectLine(position.x, position.y, position.x + direction.x * radius, position.y + direction.y * radius, 1f / 8);
	}
	
	@Override
	public Fixture getFixture()
	{
		return fixture;
	}
	
	@Override
	public EntityCreature getEntity()
	{
		return entity;
	}
	
	public Color getLineColor()
	{
		return entity.isDead() ? Color.GRAY : Color.WHITE;
	}
	
	@Override
	public void getColor(Color color)
	{
		if(entity.isDead())
			color.set(Color.DARK_GRAY);
		else
			entity.getBodyColor(color);
	}
	
	@Override
	public void dispose()
	{
		
	}
}
