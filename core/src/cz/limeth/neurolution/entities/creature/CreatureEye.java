package cz.limeth.neurolution.entities.creature;

import java.util.function.Supplier;

import neuroNet.limeth.network.adapters.InputNeuronAdapter;
import neuroNet.limeth.network.adapters.OutputResetListener;
import neuroNet.limeth.network.neurons.InputNeuron;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import cz.limeth.neurolution.entities.SComponent;
import cz.limeth.neurolution.entities.SEntity;
import cz.limeth.neurolution.entities.SEntitySet;
import cz.limeth.neurolution.entities.SRenderable;
import cz.limeth.neurolution.screen.Simulation;
import cz.limeth.neurolution.util.MathHelper;
import cz.limeth.neurolution.util.SEntityRayCastCallback;

public class CreatureEye extends InputNeuronAdapter implements SRenderable, OutputResetListener
{
	private static final float[] $vertices = new float[8];
	private static final Color $color = new Color();
	private final EntityCreature entity;
	private final float angle;
	private final int index;
	private Color lazyVisibleColor;
	
	public CreatureEye(EntityCreature entity, float angle, int index)
	{
		this.entity = entity;
		this.angle = angle;
		this.index = index;
	}
	
	public static CreatureEye[] create(EntityCreature entity)
	{
		DNA dna = entity.getDNA();
		float bodyRadius = dna.getRadius();
		EyeDefinition[] eyeDefinitions = dna.getEyeDefinitions();
		CreatureEye[] adapters = new CreatureEye[eyeDefinitions.length];
		float startAngle = 0;
		
		for(int i = 1; i < eyeDefinitions.length; i++)
		{
			EyeDefinition definition = eyeDefinitions[i];
			float angle = definition.getRequiredAngle(bodyRadius);
			startAngle -= angle;
		}
		
		startAngle *= 1 / 4f;
		
		for(int i = 0; i < eyeDefinitions.length; i++)
		{
			EyeDefinition definition = eyeDefinitions[i];
			float angle = definition.getRequiredAngle(bodyRadius);
			
			adapters[i] = new CreatureEye(entity, startAngle + startAngle, i);
			startAngle += angle / 4;
			
			if(i + 1 < eyeDefinitions.length)
				startAngle += eyeDefinitions[i + 1].getRequiredAngle(bodyRadius) / 4;
		}
		
		return adapters;
	}
	
	public static void getVertices(float[] vertices, float angle, float width, float bodyRadius, float x, float y)
	{
		if(vertices.length != 8)
			throw new IllegalArgumentException("The array must be of length 8!");
		
		float distanceFromCenter = bodyRadius * 2 / 3;
		vertices[0] = bodyRadius;
		vertices[1] = width / 2;
		vertices[2] = bodyRadius;
		vertices[3] = -width / 2;
		vertices[4] = distanceFromCenter;
		vertices[5] = ((distanceFromCenter) / bodyRadius) * width / 2;
		vertices[6] = distanceFromCenter;
		vertices[7] = -((distanceFromCenter) / bodyRadius) * width / 2;
		
		MathHelper.rotateAll(vertices, angle);
		
		for(int i = 0; i < 4; i ++)
		{
			vertices[2 * i] += x;
			vertices[2 * i + 1] += y;
		}
	}
	
	@Override
	public void setSuppliers(InputNeuron... neurons)
	{
		neurons[0].setSupplier(supplierA);
		neurons[1].setSupplier(supplierB);
		neurons[2].setSupplier(supplierC);
		
		neurons[0].setOutputResetListener(this);
		neurons[1].setOutputResetListener(this);
		neurons[2].setOutputResetListener(this);
	}
	
	private final Supplier<Float> supplierA = new Supplier<Float>()
	{
		@Override
		public Float get()
		{
			getVisibleColor($color);
			
			return getEyeDefinition().getColorTranslationAbility().getA($color);
		}
	};
	
	private final Supplier<Float> supplierB = new Supplier<Float>()
	{
		@Override
		public Float get()
		{
			getVisibleColor($color);
			
			return getEyeDefinition().getColorTranslationAbility().getB($color);
		}
	};
	
	private final Supplier<Float> supplierC = new Supplier<Float>()
	{
		@Override
		public Float get()
		{
			getVisibleColor($color);
			
			return getEyeDefinition().getColorTranslationAbility().getC($color);
		}
	};
	
	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		Body body = entity.getBody();
		DNA dna = entity.getDNA();
		EyeDefinition definition = getEyeDefinition();
		float width = definition.getWidth();
		float bodyRadius = dna.getRadius();
		Vector2 position = body.getPosition();
		float angle = MathHelper.normalizeAngle(body.getAngle() + this.angle);
		getVertices($vertices, angle, width, bodyRadius, position.x, position.y);
		getVisibleColor($color);
		
		if($vertices.length < 8)
			$color.set(Color.WHITE);
		
		renderer.setColor($color);
		renderer.triangle($vertices[0], $vertices[1], $vertices[2], $vertices[3], $vertices[4], $vertices[5]);
		renderer.triangle($vertices[4], $vertices[5], $vertices[6], $vertices[7], $vertices[2], $vertices[3]);
		
		if(Simulation.getInstance().isDebug())
			renderer.rectLine(position, getDestination(), 0.01f);
	}
	
	public void getVisibleColor(Color color)
	{
		if(lazyVisibleColor == null)
		{
			lazyVisibleColor = new Color();
			rayCast(lazyVisibleColor);
		}
		
		color.set(lazyVisibleColor);
	}
	
	private void rayCast(Color color)
	{
		Body body = entity.getBody();
		Simulation simulation = Simulation.getInstance();
		final SEntitySet entities = simulation.getEntities();
		World world = body.getWorld();
		Vector2 position = body.getPosition();
		Vector2 destination = getDestination();
		SEntityRayCastCallback callback = new SEntityRayCastCallback(body, entities);
		
		world.rayCast(callback, position, destination);
		
		SEntity hitEntity = callback.getHitEntity();
		
		if(hitEntity == null)
		{
			color.set(Color.BLACK);
			return;
		}
		
		Vector2 hitPoint = callback.getHitPoint();
		SComponent hitComponent = callback.getHitComponent();
		float travelledDistanceSquared = position.dst2(hitPoint);
		float maxDistanceSquared = position.dst2(destination);
		float fade = 1 - travelledDistanceSquared / maxDistanceSquared; //Achieves a slow fade out
		
		hitComponent.getColor(color);
		color.mul(fade);
	}
	
	public Vector2 getDestination()
	{
		Body body = entity.getBody();
		Vector2 position = body.getPosition();
		float angle = MathHelper.normalizeAngle(getAngle() + body.getAngle());
		EyeDefinition definition = getEyeDefinition();;
		float viewDistance = definition.getViewDistance();
		
		return new Vector2(position.x + MathUtils.cos(angle) * viewDistance, position.y + MathUtils.sin(angle) * viewDistance);
	}
	
	public float getAngle()
	{
		return angle;
	}
	
	public EyeDefinition getEyeDefinition()
	{
		return entity.getDNA().getEyeDefinition(index);
	}
	
	@Override
	public void onOuputReset()
	{
		lazyVisibleColor = null;
	}
	
	public void getColor(Color color)
	{
		entity.getBodyColor(color);
	}
	
	public SEntity getEntity()
	{
		return entity;
	}
	
	public int getIndex()
	{
		return index;
	}
}
