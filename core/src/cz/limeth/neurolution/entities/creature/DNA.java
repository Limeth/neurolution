package cz.limeth.neurolution.entities.creature;

import java.util.Objects;
import java.util.function.Supplier;

import neuroNet.limeth.network.NeuralConnection;
import neuroNet.limeth.network.NeuralConnectionSet;
import neuroNet.limeth.network.NeuralLayer;
import neuroNet.limeth.network.NeuralNetwork;
import neuroNet.limeth.network.functions.ActivationFunction;
import neuroNet.limeth.network.neurons.InputNeuron;
import neuroNet.limeth.network.neurons.Neuron;
import neuroNet.limeth.network.neurons.OutputNeuron;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.google.gson.annotations.Expose;

import cz.limeth.neurolution.neurons.InputEnergySupplier;
import cz.limeth.neurolution.neurons.InputRandomSupplier;
import cz.limeth.neurolution.neurons.OutputColorAdapter;

public class DNA
{
	@Expose private float size, maxRotation, maxVelocity;
	@Expose private EyeDefinition[] eyeDefinitions;
	@Expose private ColorTranslationAbility appearenceTranslationAbility;
	@Expose private NeuralNetwork network;
	private OutputColorAdapter outputColorAdapter;
	private OutputNeuron rotationNeuron;
	private OutputNeuron velocityNeuron;
	private Float lazyRadius;
	
	public DNA(float size, float maxRotation, float maxVelocity, EyeDefinition[] eyeDefinitions,
			ColorTranslationAbility appearenceTranslationAbility, NeuralNetwork network)
	{
		Objects.requireNonNull(eyeDefinitions, "The eye definitions must not be null!");
		Objects.requireNonNull(appearenceTranslationAbility, "The appearence translation ability must not be null!");
		Objects.requireNonNull(network, "The network must not be null!");
		
		this.size = size;
		this.maxRotation = maxRotation;
		this.maxVelocity = maxVelocity;
		this.eyeDefinitions = eyeDefinitions;
		this.appearenceTranslationAbility = appearenceTranslationAbility;
		this.network = network;
		this.outputColorAdapter = new OutputColorAdapter(new Supplier<ColorTranslationAbility>() {
			@Override
			public ColorTranslationAbility get()
			{
				return getAppearenceTranslationAbility();
			}
		});
	}
	
	public static DNA construct(float size, float maxRotation, float maxVelocity, int eyeAmount,
			ColorTranslationAbility appearenceTranslationAbility, int[] hiddenLayers)
	{
		EyeDefinition[] eyeDefinitions = new EyeDefinition[eyeAmount];
		
		for(int i = 0; i < eyeAmount; i++)
			eyeDefinitions[i] = new EyeDefinition(0.025f + MathUtils.random() * 0.2f, 5 + (int) (45 * Math.random()), ColorTranslationAbility.random());
		
		NeuralNetwork network = new NeuralNetwork(ActivationFunction.SIGMOID);
		
		NeuralLayer.createSimpleInputLayer(network, null, getRequiredInputAmount(eyeAmount), true);
		
		for(int i = 0; i < hiddenLayers.length; i++)
			NeuralLayer.createSimpleHiddenLayer(network, null, hiddenLayers[i], true);
		
		NeuralLayer.createSimpleOutputLayer(network, null, getRequiredOutputAmount());
		
		return new DNA(size, maxRotation, maxVelocity, eyeDefinitions, appearenceTranslationAbility, network);
	}
	
	public static DNA merge(DNA a, DNA b)
	{
		if(a.getEyeAmount() != b.getEyeAmount())
			throw new IllegalArgumentException("The DNAs have a different eye amount.");
		
		float size = getRandomInRange(a.size, b.size);
		float maxRotation = getRandomInRange(a.maxRotation, b.maxRotation);
		float maxVelocity = getRandomInRange(a.maxVelocity, b.maxVelocity);
		EyeDefinition[] eyeDefinitions = new EyeDefinition[a.getEyeAmount()];
		
		for(int i = 0; i < eyeDefinitions.length; i++)
			eyeDefinitions[i] = new EyeDefinition(
					getRandomInRange(a.eyeDefinitions[i].getWidth(), b.eyeDefinitions[i].getWidth()),
					getRandomInRange(a.eyeDefinitions[i].getViewDistance(), b.eyeDefinitions[i].getViewDistance()),
					getRandomOf(a.eyeDefinitions[i].getColorTranslationAbility(), b.eyeDefinitions[i].getColorTranslationAbility())
				);
		
		ColorTranslationAbility appearenceTranslationAbility = getRandomOf(a.appearenceTranslationAbility, b.appearenceTranslationAbility);
		NeuralNetwork network = mergeNetworks(a.network, b.network);
		
		return new DNA(size, maxRotation, maxVelocity, eyeDefinitions, appearenceTranslationAbility, network);
	}
	
	private static NeuralNetwork mergeNetworks(NeuralNetwork a, NeuralNetwork b)
	{
		NeuralNetwork result = a.clone();
		
		//Rewrite connections
		for(int layerIndex = 0; layerIndex < result.size(); layerIndex++)
		{
			NeuralLayer resultLayer = result.get(layerIndex);
			NeuralLayer bLayer = b.get(layerIndex);
			
			for(int neuronIndex = 0; neuronIndex < resultLayer.size(); neuronIndex++)
			{
				Neuron resultNeuron = resultLayer.get(neuronIndex);
				Neuron bNeuron = bLayer.get(neuronIndex);
				NeuralConnectionSet resultProvidedConnections = resultNeuron.getProvidedConnections();
				
				for(NeuralConnection resultProvidedConnection : resultProvidedConnections)
				{
					Neuron resultAcceptingNeuron = resultProvidedConnection.getOther(resultNeuron);
					NeuralLayer resultAcceptingLayer = resultAcceptingNeuron.getLayer();
					NeuralLayer bAcceptingLayer = b.get(resultAcceptingLayer.getIndex());
					Neuron bAcceptingNeuron = bAcceptingLayer.get(resultAcceptingNeuron.getIndex());
					NeuralConnection bConnection = bNeuron.getConnection(bAcceptingNeuron);
					float aWeight = resultProvidedConnection.getWeight();
					float bWeight = bConnection.getWeight();
					
					resultProvidedConnection.setWeight(getRandomInRange(aWeight, bWeight));
				}
			}
		}
		
		return result;
	}
	
	private static float getRandomInRange(float a, float b)
	{
		float min = Math.min(a, b);
		float max = Math.max(a,  b);
		float absDelta = max - min;
		float random = MathUtils.random();
		
		return min + absDelta * random;
	}
	
	private static <T> T getRandomOf(T... array)
	{
		return array[MathUtils.random(array.length - 1)];
	}
	
	public static DNA construct()
	{
		return construct(MathUtils.random(0.5f, 2), MathUtils.PI2 / 32, 1000, 7, ColorTranslationAbility.HSB, new int[] {10, 10});
	}
	
	public DNA mutate()
	{
		//if(testMutation()) this.size += MathUtils.random(- Math.min(size, 1), 1);
		if(testMutation()) this.maxRotation += MathUtils.random(- Math.min(maxRotation, 1), 1);
		if(testMutation()) this.maxVelocity += MathUtils.random(- Math.min(maxVelocity, 1), 1) * 100;
		if(testMutation()) this.appearenceTranslationAbility = ColorTranslationAbility.random();
		
		for(EyeDefinition definition : eyeDefinitions)
			if(testMutation()) definition.setColorTranslationAbility(ColorTranslationAbility.random());
		
		for(NeuralConnection connection : network.getConnections())
			if(testMutation()) connection.addWeight(MathUtils.random(-1, 1));
		
		return this;
	}
	
	private boolean testMutation()
	{
		return Math.random() < 0.1;
	}
	
	public void connectTo(EntityCreature creature)
	{
		CreatureEye[] eyes = creature.getEyes();
		NeuralLayer inputLayer = network.getFirst();
		NeuralLayer outputLayer = network.getLast();
		int eyeAmount = getEyeAmount();
		
		//Input
		((InputNeuron) inputLayer.get(0)).setSupplier(InputRandomSupplier.getInstance());
		((InputNeuron) inputLayer.get(1)).setSupplier(new InputEnergySupplier(creature));
		
		for(int i = 0; i < eyeAmount; i++)
		{
			int index = 2 + i * 3;
			CreatureEye eye = eyes[i];
			
			eye.setSuppliers(
					(InputNeuron) inputLayer.get(index),
					(InputNeuron) inputLayer.get(index + 1),
					(InputNeuron) inputLayer.get(index + 2)
				);
		}
		
		//Output
		rotationNeuron = (OutputNeuron) outputLayer.get(0);
		velocityNeuron = (OutputNeuron) outputLayer.get(1);
		outputColorAdapter.setNeurons(
				(OutputNeuron) outputLayer.get(2),
				(OutputNeuron) outputLayer.get(3),
				(OutputNeuron) outputLayer.get(4)
			);
	}
	
	public int getRequiredInputAmount()
	{
		return getRequiredInputAmount(getEyeAmount());
	}
	
	public static int getRequiredInputAmount(int visionAdapterAmount)
	{
		return 2 + visionAdapterAmount * 3; //Random, Energy
	}
	
	public static int getRequiredOutputAmount()
	{
		return 5; //Rotation, Velocity, Color
	}
	
	public float decideRelativeRotation()
	{
		return (rotationNeuron.getOutput() * 2 - 1) * getMaxRotation();
	}
	
	public float decideRelativeVelocity()
	{
		return (velocityNeuron.getOutput() * 2 - 1) * getMaxVelocity();
	}
	
	public void decideOutputColor(Color color)
	{
		outputColorAdapter.getValue(color);
	}
	
	public float getSize()
	{
		return size;
	}
	
	private float initRadius()
	{
		return lazyRadius = (float) Math.sqrt(size / Math.PI);
	}
	
	public float getRadius()
	{
		return lazyRadius != null ? lazyRadius : initRadius();
	}

	public float getMaxRotation()
	{
		return maxRotation;
	}

	public float getMaxVelocity()
	{
		return maxVelocity;
	}

	public NeuralNetwork getNetwork()
	{
		return network;
	}
	
	public OutputNeuron getRotationNeuron()
	{
		return rotationNeuron;
	}
	
	public OutputNeuron getVelocityNeuron()
	{
		return velocityNeuron;
	}
	
	public int getEyeAmount()
	{
		return eyeDefinitions.length;
	}
	
	public EyeDefinition getEyeDefinition(int index)
	{
		return eyeDefinitions[index];
	}
	
	public EyeDefinition[] getEyeDefinitions()
	{
		return eyeDefinitions;
	}
	
	public ColorTranslationAbility getAppearenceTranslationAbility()
	{
		return appearenceTranslationAbility;
	}
	
	public OutputColorAdapter getOutputColorAdapter()
	{
		return outputColorAdapter;
	}
}
