package cz.limeth.neurolution.entities.creature;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;

import cz.limeth.neurolution.util.MathHelper;

public enum ColorTranslationAbility
{
	HSB {
		@Override
		public float getA(Color color)
		{
			return MathHelper.getHue(color);
		}

		@Override
		public float getB(Color color)
		{
			return MathHelper.getSaturation(color);
		}

		@Override
		public float getC(Color color)
		{
			return MathHelper.getBrightness(color);
		}
		
		@Override
		public void getColor(float a, float b, float c, Color color)
		{
			color.set(java.awt.Color.HSBtoRGB(a, b, c) << 8);
		}
	}, RGB {
		@Override
		public float getA(Color color)
		{
			return color.r;
		}

		@Override
		public float getB(Color color)
		{
			return color.g;
		}

		@Override
		public float getC(Color color)
		{
			return color.b;
		}
		
		@Override
		public void getColor(float a, float b, float c, Color color)
		{
			color.r = a;
			color.g = b;
			color.b = c;
			color.a = 1;
		}
	};
	
	public abstract float getA(Color color);
	public abstract float getB(Color color);
	public abstract float getC(Color color);
	public abstract void getColor(float a, float b, float c, Color color);
	
	public Color getColor(float a, float b, float c)
	{
		Color color = new Color();
		
		getColor(a, b, c, color);
		
		return color;
	}
	
	public static ColorTranslationAbility random()
	{
		ColorTranslationAbility[] values = values();
		
		return values[MathUtils.random(values.length - 1)];
	}
}
