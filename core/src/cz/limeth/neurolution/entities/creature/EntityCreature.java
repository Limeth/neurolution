package cz.limeth.neurolution.entities.creature;

import java.util.Arrays;
import java.util.function.Consumer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import cz.limeth.neurolution.NConst;
import cz.limeth.neurolution.entities.SComponent;
import cz.limeth.neurolution.entities.SEntity;
import cz.limeth.neurolution.entities.SSpawnable;
import cz.limeth.neurolution.throwables.SpawnException;
import cz.limeth.neurolution.util.BodyBuilder;
import cz.limeth.neurolution.util.BodyDefBuilder;
import cz.limeth.neurolution.util.MathHelper;

public class EntityCreature implements SEntity, SSpawnable
{
	private final DNA dna;
	private Body body;
	private ComponentBody componentBody;
	private CreatureEye[] eyes;
	private float energy;
	private Float lazyMaxEnergy;
	private Long spawnedAt;
	private int lifetime;
	private Consumer<EntityCreature> onDeath;
	
	public EntityCreature(DNA dna, Consumer<EntityCreature> onDeath)
	{
		this.dna = dna;
		this.onDeath = onDeath;
		this.energy = getMaxEnergy() * 2 / 3;
	}
	
	public EntityCreature(Consumer<EntityCreature> onDeath)
	{
		this(DNA.construct(), onDeath);
	}
	
	public EntityCreature()
	{
		this(null);
	}
	
	@Override
	public EntityCreature spawn(World world, final Vector2 position)
	{
		if(isSpawned())
			throw new SpawnException(this, this + " has been already spawned!");
		
		body = new BodyBuilder().bodyDef(
				new BodyDefBuilder().position(position).type(BodyType.DynamicBody).build()
			).world(world).build();
		
		componentBody = new ComponentBody(this);
		eyes = CreatureEye.create(this);
		dna.connectTo(this);
		
		lifetime = 0;
		spawnedAt = TimeUtils.millis();
		return this;
	}
	
	@Override
	public void despawn()
	{
		if(!isSpawned())
			throw new SpawnException(this, this + " has not been yet spawned!");
		
		World world = body.getWorld();
		
		if(!isDead())
			die();
		
		world.destroyBody(body);
		componentBody.dispose();
		
		body = null;
		componentBody = null;
		spawnedAt = null;
	}
	
	public void die()
	{
		if(onDeath != null)
			onDeath.accept(this);
	}
	
	@Override
	public Long getSpawnedAt()
	{
		return spawnedAt;
	}
	
	@Override
	public boolean isSpawned()
	{
		return spawnedAt != null;
	}
	
	public boolean isDead()
	{
		return energy <= 0 || energy > getMaxEnergy();
	}
	
	public void decreaseDeltaEnergyPerStep()
	{
		setEnergy(energy - outputDeltaEnergyPerStep());
	}
	
	public float outputDeltaEnergyPerStep()
	{
		float rotationEnergy = dna.decideRelativeRotation();
		float velocityEnergy = dna.decideRelativeVelocity();
		
		if(rotationEnergy < 0)
			rotationEnergy *= -1;
		
		if(velocityEnergy < 0)
			velocityEnergy *= -1;
		
		return 1 + 3 * (rotationEnergy + velocityEnergy / 1000 + dna.getEyeAmount() / 10f);
	}
	
	public float initMaxEnergy()
	{
		float radius = dna.getRadius();
		
		return lazyMaxEnergy = MathUtils.PI * radius * radius * NConst.CREATURE_MAX_ENERGY_MODIFIER;
	}
	
	public float getMaxEnergy()
	{
		return lazyMaxEnergy != null ? lazyMaxEnergy : initMaxEnergy();
	}
	
	public float getEnergyPercentage()
	{
		return energy / getMaxEnergy();
	}
	
	@Override
	public Body getBody()
	{
		return body;
	}
	
	@Override
	public void step(float delta)
	{
		if(!isSpawned() || isDead())
			return;
		
		dna.getNetwork().resetOutputs();
		
		float currentRotation = body.getAngularVelocity();
		float rotation = dna.decideRelativeRotation() * (1 + delta) - currentRotation;
		float velocity = dna.decideRelativeVelocity() * (1 + delta);
		Vector2 direction = getDirection();
		direction.x *= velocity;
		direction.y *= velocity;
		direction.add(body.getLinearVelocity().scl(-1));
		
		body.applyTorque(rotation, true);
		body.applyLinearImpulse(direction, body.getWorldCenter(), true);
		decreaseDeltaEnergyPerStep();
		
		if(!isDead())
			lifetime++;
	}
	
	@Override
	public void render(ShapeRenderer renderer, SpriteBatch polygonSpriteBatch, Matrix4 matrix, float delta)
	{
		if(!isSpawned())
			return;
		
		componentBody.render(renderer, polygonSpriteBatch, matrix, delta);
		
		if(isDead())
			return;
		
		for(CreatureEye eye : eyes)
			eye.render(renderer, polygonSpriteBatch, matrix, delta);
	}
	
	public Vector2 getDirection()
	{
		float angle = body.getAngle();
		float normalAngle = MathHelper.normalizeAngle(angle);
		
		return new Vector2(MathUtils.cos(normalAngle), MathUtils.sin(normalAngle));
	}
	
	public ComponentBody getComponentBody()
	{
		return componentBody;
	}
	
	public void getBodyColor(Color color)
	{
		dna.decideOutputColor(color);
	}
	
	public float getEnergy()
	{
		return energy;
	}
	
	public void setEnergy(float energy)
	{
		boolean dead = isDead();
		
		if(energy < 0)
			this.energy = 0;
		else
			this.energy = energy;
		
		if(!dead && isDead())
			die();
	}
	
	public float increaseEnergy(float amount)
	{
		setEnergy(energy + amount);
		
		return energy;
	}

	@Override
	public SComponent[] getComponents()
	{
		return new SComponent[] {componentBody};
	}

	@Override
	public SComponent getComponent(Fixture fixture)
	{
		if(componentBody.getFixture() == fixture)
			return componentBody;
		
		return null;
	}
	
	@Override
	public void dispose()
	{
		if(isSpawned())
			despawn();
	}
	
	public int getLifetime()
	{
		return lifetime;
	}
	
	public Consumer<EntityCreature> getOnDeath()
	{
		return onDeath;
	}
	
	public void setOnDeath(Consumer<EntityCreature> onDeath)
	{
		this.onDeath = onDeath;
	}
	
	public CreatureEye[] getEyes()
	{
		return eyes;
	}
	
	public DNA getDNA()
	{
		return dna;
	}

	@Override
	public String toString()
	{
		return "EntityCreature [dna=" + dna + ", body=" + body + ", componentBody=" + componentBody + ", eyes=" + Arrays.toString(eyes) + ", energy=" + energy + ", lazyMaxEnergy=" + lazyMaxEnergy + ", spawnedAt=" + spawnedAt + ", lifetime=" + lifetime + ", onDeath=" + onDeath + "]";
	}
}
