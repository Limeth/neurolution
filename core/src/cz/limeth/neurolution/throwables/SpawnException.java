package cz.limeth.neurolution.throwables;

import cz.limeth.neurolution.entities.SSpawnable;

public class SpawnException extends RuntimeException
{
	private static final long serialVersionUID = 2129940832157879779L;
	private final SSpawnable spawnable;
	
	public SpawnException(SSpawnable spawnable, String message)
	{
		super(message);
		
		this.spawnable = spawnable;
	}
	
	public SSpawnable getSpawnable()
	{
		return spawnable;
	}
}
