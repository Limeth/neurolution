package cz.limeth.neurolution.neurons;

import java.util.function.Supplier;

import com.badlogic.gdx.math.MathUtils;

public class InputRandomSupplier implements Supplier<Float>
{
	public static InputRandomSupplier instance = new InputRandomSupplier();
	
	private InputRandomSupplier() {}
	
	@Override
	public Float get()
	{
		return MathUtils.random() * 2 - 1;
	}
	
	public static InputRandomSupplier getInstance()
	{
		return instance;
	}
}
