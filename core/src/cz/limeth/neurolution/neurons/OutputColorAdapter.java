package cz.limeth.neurolution.neurons;

import java.util.function.Supplier;

import neuroNet.limeth.network.adapters.OutputNeuronAdapter;
import neuroNet.limeth.network.neurons.OutputNeuron;

import com.badlogic.gdx.graphics.Color;

import cz.limeth.neurolution.entities.creature.ColorTranslationAbility;

public class OutputColorAdapter extends OutputNeuronAdapter<Color>
{
	private Supplier<ColorTranslationAbility> colorTranslationAbilitySupplier;
	
	public OutputColorAdapter(Supplier<ColorTranslationAbility> colorTranslationAbilitySupplier, OutputNeuron a, OutputNeuron b, OutputNeuron c)
	{
		super(a, b, c);
		
		this.colorTranslationAbilitySupplier = colorTranslationAbilitySupplier;
	}
	
	public OutputColorAdapter(Supplier<ColorTranslationAbility> colorTranslationAbilitySupplier)
	{
		this(colorTranslationAbilitySupplier, null, null, null);
	}
	
	public void setColorTranslationAbility(Supplier<ColorTranslationAbility> colorTranslationAbilitySupplier)
	{
		this.colorTranslationAbilitySupplier = colorTranslationAbilitySupplier;
	}
	
	public Supplier<ColorTranslationAbility> getColorTranslationAbilitySupplier()
	{
		return colorTranslationAbilitySupplier;
	}
	
	public void setColorTranslationAbilitySupplier(Supplier<ColorTranslationAbility> colorTranslationAbilitySupplier)
	{
		this.colorTranslationAbilitySupplier = colorTranslationAbilitySupplier;
	}
	
	@Override
	public Color getValue()
	{
		return colorTranslationAbilitySupplier.get().getColor(
					getNeuron(0).getOutput(),
					getNeuron(1).getOutput(),
					getNeuron(2).getOutput()
				);
	}
	
	public void getValue(Color color)
	{
		colorTranslationAbilitySupplier.get().getColor(
				getNeuron(0).getOutput(),
				getNeuron(1).getOutput(),
				getNeuron(2).getOutput(),
				color
			);
	}
}
