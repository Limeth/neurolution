package cz.limeth.neurolution.neurons;

import java.util.function.Supplier;

import cz.limeth.neurolution.entities.creature.EntityCreature;

public class InputEnergySupplier implements Supplier<Float>
{
	private EntityCreature creature;
	
	public InputEnergySupplier(EntityCreature creature)
	{
		this.creature = creature;
	}
	
	@Override
	public Float get()
	{
		return creature.getEnergyPercentage();
	}
	
	public EntityCreature getCreature()
	{
		return creature;
	}
	
	public void setCreature(EntityCreature creature)
	{
		this.creature = creature;
	}
}
