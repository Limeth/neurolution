package cz.limeth.neurolution.evolution;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.entities.creature.DNA;
import cz.limeth.neurolution.entities.creature.DNAResult;

public class SimpleEvolutionHandler implements EvolutionHandler
{
	private EvolutionExecutor executor;
	private EvolutionFilter filter;
	private final HashSet<DNA> batch;
	private Integer dnaPerBatch;
	private Integer generation;
	
	public SimpleEvolutionHandler(Integer dnaPerBatch, EvolutionExecutor executor, EvolutionFilter filter)
	{
		this.executor = executor;
		this.filter = filter;
		
		executor.setHandler(this);
		filter.setHandler(this);
		
		batch = new HashSet<DNA>();
		this.dnaPerBatch = dnaPerBatch;
	}
	
	public SimpleEvolutionHandler()
	{
		this(null, null, null);
	}
	
	public static SimpleEvolutionHandler newDefault()
	{
		return new SimpleEvolutionHandler(20, new LinearEvolutionExecutor(4, 1 / 1000f), new LinearEvolutionFilter(0.5f));
	}

	@Override
	public void execute()
	{
		if(generation != null)
			generation++;
		else
			generation = 0;
		
		System.out.println("Generation: " + generation);
		
		executor.execute();
	}

	@Override
	public void filter(Collection<DNAResult> results)
	{
		HashSet<DNA> batch = filter.filter(results);
		
		setBatch(batch);
	}

	@Override
	public HashSet<DNA> getBatch()
	{
		while(batch.size() < dnaPerBatch)
			batch.add(DNA.construct());
		
		return batch;
	}

	@Override
	public void setBatch(Collection<DNA> batch)
	{
		this.batch.clear();
		this.batch.addAll(batch);
	}
	
	@Override
	public void deserialize(JsonObject root, JsonDeserializationContext context) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		if(root.has("dnaPerBatch"))
			dnaPerBatch = root.get("dnaPerBatch").getAsInt();

		Class<?> executorClass = Class.forName(root.get("executor").getAsJsonObject().get("class").getAsString());
		executor = (EvolutionExecutor) executorClass.newInstance();
		executor.deserialize(root.get("executor").getAsJsonObject().get("data").getAsJsonObject(), context);
		executor.setHandler(this);
		
		Class<?> filterClass = Class.forName(root.get("filter").getAsJsonObject().get("class").getAsString());
		filter = (EvolutionFilter) filterClass.newInstance();
		filter.deserialize(root.get("filter").getAsJsonObject().get("data").getAsJsonObject(), context);
		filter.setHandler(this);
		
		Iterator<JsonElement> batchIterator = root.get("batch").getAsJsonArray().iterator();
		
		batch.clear();
		
		while(batchIterator.hasNext())
			batch.add((DNA) context.deserialize(batchIterator.next(), DNA.class));
	}

	@Override
	public JsonObject serialize(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		JsonObject executorObject = new JsonObject();
		
		executorObject.addProperty("class", executor.getClass().getCanonicalName());
		executorObject.add("data", executor.serialize(context));
		
		JsonObject filterObject = new JsonObject();
		
		filterObject.addProperty("class", filter.getClass().getCanonicalName());
		filterObject.add("data", filter.serialize(context));
		
		root.addProperty("dnaPerBatch", dnaPerBatch);
		root.add("executor", executorObject);
		root.add("filter", filterObject);
		root.add("batch", context.serialize(batch));
		
		return root;
	}

	@Override
	public EvolutionExecutor getExecutor()
	{
		return executor;
	}

	@Override
	public EvolutionFilter getFilter()
	{
		return filter;
	}
	
	public int getDnaPerBatch()
	{
		return dnaPerBatch;
	}
}
