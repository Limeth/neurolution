package cz.limeth.neurolution.evolution;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

public interface EvolutionExecutor
{
	public void execute();
	public void nextIteration();
	public EvolutionHandler getHandler();
	public void setHandler(EvolutionHandler handler);
	public void deserialize(JsonObject root, JsonDeserializationContext context);
	public JsonObject serialize(JsonSerializationContext context);
}
