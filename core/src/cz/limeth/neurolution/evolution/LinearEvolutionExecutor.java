package cz.limeth.neurolution.evolution;

import java.util.HashSet;
import java.util.Iterator;
import java.util.function.Consumer;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.NConst;
import cz.limeth.neurolution.entities.EntityFood;
import cz.limeth.neurolution.entities.SEntity;
import cz.limeth.neurolution.entities.SEntitySet;
import cz.limeth.neurolution.entities.creature.DNA;
import cz.limeth.neurolution.entities.creature.DNAResult;
import cz.limeth.neurolution.entities.creature.EntityCreature;
import cz.limeth.neurolution.screen.Simulation;


public class LinearEvolutionExecutor implements EvolutionExecutor
{
	private EvolutionHandler handler;
	private final HashSet<DNAResult> results;
	private Integer precision;
	private Integer iteration;
	private Float foodDensity;
	
	public LinearEvolutionExecutor(Integer precision, Float foodDensity)
	{
		results = new HashSet<DNAResult>();
		this.precision = precision;
		this.foodDensity = foodDensity;
	}
	
	public LinearEvolutionExecutor()
	{
		this(null, null);
	}
	
	@Override
	public void execute()
	{
		iteration = null;
		results.clear();
		nextIteration();
	}

	@Override
	public void nextIteration()
	{
		if(iteration != null)
		{
			iteration++;
			
			if(generationEnded())
			{
				EvolutionFilter filter = handler.getFilter();
				
				filter.filter(results);
				handler.execute();
				return;
			}
		}
		else
			iteration = 0;
		
		System.out.println("Iteration: " + iteration);
		
		removeEntities();
		spawnAllFood();
		spawnCreatures();
	}
	
	private void removeEntities()
	{
		Simulation simulation = Simulation.getInstance();
		SEntitySet entities = simulation.getEntities();
		Iterator<SEntity> entityIterator = entities.iterator();
		
		while(entityIterator.hasNext())
		{
			SEntity entity = entityIterator.next();
			
			entity.dispose();
			entityIterator.remove();
		}
	}
	
	private void spawnAllFood()
	{
		Simulation simulation = Simulation.getInstance();
		float width = simulation.getWidth();
		float height = simulation.getHeight();
		float area = width * height;
		int amount = (int) Math.ceil(area * foodDensity);
		
		for(int i = 0; i < amount; i++)
			spawnFood();
	}
	
	private void spawnFood()
	{
		Simulation simulation = Simulation.getInstance();
		SEntitySet entities = simulation.getEntities();
		World world = simulation.getWorld();
		float x = (float) Math.random() * simulation.getWidth();
		float y = (float) Math.random() * simulation.getHeight();
		Vector2 position = new Vector2(x, y);
		float size = NConst.RANDOM.nextInt(5) + 1;
		EntityFood food = new EntityFood(size, 1000, onFoodDecay);
		
		entities.add(food);
		food.spawn(world, position);
	}
	
	private void spawnCreatures()
	{
		Simulation simulation = Simulation.getInstance();
		World world = simulation.getWorld();
		SEntitySet entities = simulation.getEntities();
		HashSet<DNA> batch = handler.getBatch();
		
		for(DNA dna : batch)
		{
			float x = (float) Math.random() * simulation.getWidth();
			float y = (float) Math.random() * simulation.getHeight();
			Vector2 position = new Vector2(x, y);
			EntityCreature creature = new EntityCreature(dna, onDeath);
			
			entities.add(creature);
			creature.spawn(world, position);
		}
	}
	
	private boolean iterationEnded()
	{
		Simulation simulation = Simulation.getInstance();
		SEntitySet entities = simulation.getEntities();
		
		for(SEntity entity : entities)
			if(entity instanceof EntityCreature)
			{
				EntityCreature creature = (EntityCreature) entity;
				
				if(!creature.isDead())
					return false;
			}
		
		return true;
	}
	
	private boolean generationEnded()
	{
		return iteration >= precision;
	}
	
	private final Consumer<EntityCreature> onDeath = new Consumer<EntityCreature>()
	{
		@Override
		public void accept(final EntityCreature creature)
		{
			Simulation.getInstance().runLater(new Runnable() {
				@Override
				public void run()
				{
					DNA dna = creature.getDNA();
					int lifetime = creature.getLifetime();
					
					for(DNAResult result : results)
					{
						DNA currentDNA = result.getDNA();
						
						if(dna.equals(currentDNA))
							result.setLifetime(lifetime + result.getLifetime());
					}
					
					if(iterationEnded()) //TODO The creature that just died might not have isDead() set to true
						nextIteration();
				}
			});
		}
	};
	
	private final Consumer<EntityFood> onFoodDecay = new Consumer<EntityFood>()
	{
		@Override
		public void accept(final EntityFood food)
		{
			Simulation.getInstance().runLater(new Runnable() {
				@Override
				public void run()
				{
					Simulation simulation = Simulation.getInstance();
					SEntitySet entities = simulation.getEntities();
					
					food.dispose();
					entities.remove(food);
					spawnFood();
					System.out.println("DECAY");
				}
			});
		}
	};
	
	@Override
	public void deserialize(JsonObject root, JsonDeserializationContext context)
	{
		precision = root.get("precision").getAsInt();
		foodDensity = root.get("foodDensity").getAsFloat();
	}
	
	@Override
	public JsonObject serialize(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		
		root.addProperty("precision", precision);
		root.addProperty("foodDensity", foodDensity);
		
		return root;
	}
	
	public int getPrecision()
	{
		return precision;
	}
	
	public void setPrecision(Integer precision)
	{
		this.precision = precision;
	}
	
	public Float getFoodDensity()
	{
		return foodDensity;
	}
	
	public void setFoodDensity(Float foodDensity)
	{
		this.foodDensity = foodDensity;
	}

	@Override
	public EvolutionHandler getHandler()
	{
		return handler;
	}
	
	@Override
	public void setHandler(EvolutionHandler handler)
	{
		this.handler = handler;
	}
}
