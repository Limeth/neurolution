package cz.limeth.neurolution.evolution;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.entities.SRenderable;

public abstract class EvolutionListener implements SRenderable
{
	public abstract void begin();
	public abstract void deserialize(JsonObject object, JsonDeserializationContext context);
	public abstract JsonObject serialize(JsonSerializationContext context);
}
