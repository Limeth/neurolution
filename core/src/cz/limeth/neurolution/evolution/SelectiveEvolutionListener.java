package cz.limeth.neurolution.evolution;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.function.Consumer;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.JsonWriter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.NConst;
import cz.limeth.neurolution.entities.EntityFood;
import cz.limeth.neurolution.entities.SEntitySet;
import cz.limeth.neurolution.entities.creature.DNA;
import cz.limeth.neurolution.entities.creature.DNAResult;
import cz.limeth.neurolution.entities.creature.EntityCreature;
import cz.limeth.neurolution.json.NeurolutionAdapterFactory;
import cz.limeth.neurolution.screen.Simulation;

public class SelectiveEvolutionListener extends EvolutionListener
{
//	static
//	{
//		BitmapFont font = new BitmapFont();
//		
//		font.setColor(Color.WHITE);
//		font.setScale(0.1f);
//		
//		FONT = font;
//	}
//	
//	private static final BitmapFont FONT;
	private final HashSet<EntityCreature> creatures = new HashSet<EntityCreature>();
	private final ArrayList<DNAResult> currentDNAs = new ArrayList<DNAResult>();
	private final ArrayList<DNAResult> futureDNAs = new ArrayList<DNAResult>();
	private int amountPerBatch, dnaPerBatch, preservedDna, batch = -1, generation = 0;
	private float maxRadius, foodDensity;
	private final FoodRunnable foodRunnable = new FoodRunnable();
	private boolean foodSpawned;
	
	public SelectiveEvolutionListener(int amountPerBatch, int dnaPerBatch, int preserverDna, float maxRadius, float foodDensity)
	{
		this.amountPerBatch = amountPerBatch;
		this.maxRadius = maxRadius;
		this.dnaPerBatch = dnaPerBatch;
		this.preservedDna = preservedDna;
		this.foodDensity = foodDensity;
	}
	
	public SelectiveEvolutionListener() {}
	
	@Override
	public void begin()
	{
		newBatch();
	}
	
	@Override
	public JsonObject serialize(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		
		root.addProperty("generation", generation);
		root.addProperty("amountPerBatch", amountPerBatch);
		root.addProperty("dnaPerBatch", dnaPerBatch);
		root.addProperty("preservedDna", preservedDna);
		root.addProperty("maxRadius", maxRadius);
		root.addProperty("foodDensity", foodDensity);
		root.add("currentDNAs", context.serialize(currentDNAs));
		root.add("futureDNAs", context.serialize(futureDNAs));
		
		return root;
	}
	
	@Override
	public void deserialize(JsonObject compressed, JsonDeserializationContext context)
	{
		JsonObject object = compressed.getAsJsonObject();
		
		generation = object.get("generation").getAsInt();
		amountPerBatch = object.get("amountPerBatch").getAsInt();
		dnaPerBatch = object.get("dnaPerBatch").getAsInt();
		preservedDna = object.get("preservedDna").getAsInt();
		maxRadius = object.get("maxRadius").getAsFloat();
		foodDensity = object.get("foodDensity").getAsFloat();
		
		JsonArray currentDNAsArray = object.get("currentDNAs").getAsJsonArray();
		JsonArray futureDNAsArray = object.get("futureDNAs").getAsJsonArray();
		
		currentDNAs.clear();
		futureDNAs.clear();
		
		for(JsonElement element : currentDNAsArray)
		{
			DNAResult result = context.deserialize(element, DNAResult.class);
			
			currentDNAs.add(result);
		}
		
		for(JsonElement element : futureDNAsArray)
		{
			DNAResult result = context.deserialize(element, DNAResult.class);
			
			futureDNAs.add(result);
		}
	}
	
	public SelectiveEvolutionListener newBatch()
	{
		synchronized(Simulation.getInstance().getLock())
		{
			return newAsyncBatch();
		}
	}
	
	public SelectiveEvolutionListener newAsyncBatch()
	{
		Simulation simulation = Simulation.getInstance();
		batch++;
		
		if(!foodSpawned)
		{
			float foodAmount = simulation.getWidth() * simulation.getHeight() * foodDensity;
			
			for(int i = 0; i < foodAmount; i++)
				foodRunnable.run();
			
			foodSpawned = true;
		}
		
		if(batch > 0 || generation > 0)
		{
			if(batch <= 0)
				futureDNAs.clear();
			
			for(int i = 0; i < dnaPerBatch && creatures.size() > 0; i++)
			{
				Integer longestLifetime = null;
				DNA bestDNA = null;
				
				for(EntityCreature creature : creatures)
				{
					int lifetime = creature.getLifetime();
					
					if(longestLifetime == null || lifetime >= longestLifetime)
					{
						longestLifetime = lifetime;
						bestDNA = creature.getDNA();
					}
				}
				
				DNAResult result = new DNAResult(bestDNA, longestLifetime);
				boolean added = false;
				
				for(int j = 0; j < futureDNAs.size(); j++)
				{
					DNAResult currentResult = futureDNAs.get(j);
					long lifetime = currentResult.getLifetime();
					
					if(lifetime <= longestLifetime)
					{
						futureDNAs.add(j, result);
						added = true;
						break;
					}
				}
				
				if(!added)
					futureDNAs.add(result);
				
				if(futureDNAs.size() >= amountPerBatch * 2)
				{
					
					currentDNAs.clear();
					currentDNAs.addAll(futureDNAs);
					futureDNAs.clear();
					generation++;
					batch = 0;
					break;
				}
			}
		}
		
		if(batch <= 0 && generation > 0)
			saveCurrentDNA();
		
		SEntitySet entities = simulation.getEntities();
		
		for(EntityCreature creature : creatures)
		{
			if(creature.isSpawned())
				creature.despawn();
			
			creature.dispose();
			entities.remove(creature);
			
			if(simulation.isSelected(creature))
				simulation.deselect();
		}
		
		creatures.clear();
		World world = simulation.getWorld();
		final float maxRadius = 2;
		
		for(int i = 0; i < amountPerBatch; i++)
		{
			float x = maxRadius + (float) Math.random() * (simulation.getWidth() - maxRadius * 2);
			float y = maxRadius + (float) Math.random() * (simulation.getHeight() - maxRadius * 2);
			Vector2 position = new Vector2(x, y);
			EntityCreature creature;
			
			if(generation <= 0)
				creature = new EntityCreature(onDeath);
			else
			{
				DNAResult a = currentDNAs.get(i * 2);
				DNAResult b = currentDNAs.get(i * 2 + 1);
				DNA merged = DNA.merge(a.getDNA(), b.getDNA()).mutate();
				creature = new EntityCreature(merged, onDeath);
			}
			
			creatures.add(creature);
			creature.spawn(world, position);
			entities.add(creature);
		}
		
		if(currentDNAs.size() > 0 && batch == 0)
		{
			DNAResult worstSelectedDNA = currentDNAs.get(currentDNAs.size() - 1);
			int shortestSelectedSeconds = worstSelectedDNA.getLifetimeSeconds();
			DNAResult bestDNA = currentDNAs.get(0);
			int longestSeconds = bestDNA.getLifetimeSeconds();
			
			System.out.println("Selected DNA lifetime range: <" + shortestSelectedSeconds + "; " + longestSeconds + "> s");
		}
		
		if(batch <= 0)
			System.out.println("Generation: " + generation);
		
		System.out.println("Batch: " + batch);
		
		return this;
	}
	
	private void saveCurrentDNA()
	{
		TimeZone tz = TimeZone.getTimeZone("UTC");
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm");
	    df.setTimeZone(tz);
	    String isoTime = df.format(new Date());
	    String fileName = "G" + generation + " " + isoTime + ".json";
	    String path = NConst.PATH_SAVES + fileName;
	    
	    Gdx.files.getFileHandle(NConst.PATH_SAVES, FileType.Local).mkdirs();
	    
	    FileHandle file = Gdx.files.getFileHandle(path, FileType.Local);
	    
	    try
		{
			System.out.println("Saving progress to: " + path);
			
			Writer writer = file.writer(false);
			Gson gson = NeurolutionAdapterFactory.createGsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
			JsonWriter jsonWriter = new JsonWriter(writer);
			
			gson.toJson(this, EvolutionListener.class, jsonWriter);
			jsonWriter.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	    
	}
	
	public boolean prepare()
	{
		EntityCreature one = null;
		
		for(EntityCreature creature : creatures)
			if(!creature.isDead())
				if(one == null)
					one = creature;
				else
					return false;
		
		if(one != null && one.isSpawned())
			one.despawn();
		
		return true;
	}
	
	@Override
	public void render(ShapeRenderer renderer, SpriteBatch spriteBatch, Matrix4 matrix, float delta)
	{
		Simulation simulation = Simulation.getInstance();
		float height = simulation.getHeight();
		String string = generateString();
		
		//renderer.flush();
		//FONT.drawMultiLine(spriteBatch, string, 0, height);
		//spriteBatch.flush();
	}
	
	public String generateString()
	{
		Simulation simulation = Simulation.getInstance();
		
		return "Generation: " + generation
				+ "\nBatch: " + batch
				+ "\nFPS: " + Gdx.graphics.getFramesPerSecond();
	}
	
	private final Runnable onDeath = new Runnable()
	{
		@Override
		public void run()
		{
			Simulation.getInstance().runLater(onDeathPost);
		}
	};
	
	private final Runnable onDeathPost = new Runnable()
	{
		@Override
		public void run()
		{
			if(prepare())
				newBatch();
		}
	};
	
	private class FoodRunnable implements Runnable, Consumer<EntityFood>
	{
		@Override
		public void accept(EntityFood removedFood)
		{
			Simulation simulation = Simulation.getInstance();
			SEntitySet entities = simulation.getEntities();
			int duration = 10 * 60;
			
			if(removedFood != null)
				entities.remove(removedFood);
			else
				duration = (int) Math.ceil(duration * Math.random());
			
			float width = simulation.getWidth();
			float height = simulation.getHeight();
			World world = simulation.getWorld();
			float x = maxRadius + (float) Math.random() * (width - maxRadius * 2);
			float y = maxRadius + (float) Math.random() * (height - maxRadius * 2);
			Vector2 position = new Vector2(x, y);
			EntityFood food = new EntityFood(MathUtils.random(1, 10), duration, this).spawn(world, position);
			
			entities.add(food);
		}

		@Override
		public Consumer<EntityFood> andThen(Consumer<? super EntityFood> arg0)
		{
			return null;
		}

		@Override
		public void run()
		{
			accept(null);
		}
	}
}
