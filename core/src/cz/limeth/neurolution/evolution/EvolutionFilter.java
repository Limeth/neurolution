package cz.limeth.neurolution.evolution;

import java.util.Collection;
import java.util.HashSet;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.entities.creature.DNA;
import cz.limeth.neurolution.entities.creature.DNAResult;

public interface EvolutionFilter
{
	public HashSet<DNA> filter(Collection<DNAResult> results);
	public EvolutionHandler getHandler();
	public void setHandler(EvolutionHandler handler);
	public void deserialize(JsonObject root, JsonDeserializationContext context);
	public JsonObject serialize(JsonSerializationContext context);
}
