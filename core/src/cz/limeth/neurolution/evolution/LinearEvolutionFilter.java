package cz.limeth.neurolution.evolution;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.entities.creature.DNA;
import cz.limeth.neurolution.entities.creature.DNAResult;

public class LinearEvolutionFilter implements EvolutionFilter
{
	private EvolutionHandler handler;
	private Float removalFraction;
	
	public LinearEvolutionFilter(Float removalFraction)
	{
		this.removalFraction = removalFraction;
	}
	
	public LinearEvolutionFilter()
	{
		this(null);
	}
	
	@Override
	public HashSet<DNA> filter(Collection<DNAResult> results)
	{
		HashSet<DNA> filtered = new HashSet<DNA>();
		int initialSize = results.size();
		
		while(results.size() - 1 > initialSize * (1 - removalFraction))
		{
			Iterator<DNAResult> iterator = results.iterator();
			DNAResult bestResultA = null;
			DNAResult bestResultB = null;
			
			while(iterator.hasNext())
			{
				DNAResult currentResult = iterator.next();
				
				if(bestResultA == null)
					bestResultA = currentResult;
				else if(bestResultB == null)
					bestResultB = currentResult;
				else if(currentResult.getLifetime() >= bestResultA.getLifetime())
				{
					bestResultB = bestResultA;
					bestResultA = currentResult;
				}
				else if(currentResult.getLifetime() >= bestResultB.getLifetime())
					bestResultB = currentResult;
			}
			
			DNA dnaA = bestResultA.getDNA();
			DNA dnaB = bestResultB.getDNA();
			DNA merged = DNA.merge(dnaA, dnaB);
			
			filtered.add(merged);
		}
		
		return filtered;
	}

	@Override
	public void deserialize(JsonObject root, JsonDeserializationContext context)
	{
		removalFraction = root.get("removalFraction").getAsFloat();
	}

	@Override
	public JsonObject serialize(JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		
		root.addProperty("removalFraction", removalFraction);
		
		return root;
	}
	
	public Float getRemovalFraction()
	{
		return removalFraction;
	}
	
	public void setRemovalFraction(Float removalFraction)
	{
		this.removalFraction = removalFraction;
	}

	@Override
	public EvolutionHandler getHandler()
	{
		return handler;
	}
	
	@Override
	public void setHandler(EvolutionHandler handler)
	{
		this.handler = handler;
	}
}
