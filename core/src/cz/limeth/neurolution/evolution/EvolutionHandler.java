package cz.limeth.neurolution.evolution;

import java.util.Collection;
import java.util.HashSet;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import cz.limeth.neurolution.entities.creature.DNA;
import cz.limeth.neurolution.entities.creature.DNAResult;

public interface EvolutionHandler
{
	/**
	 * Starts a new iteration resulting in a mutated {@link DNAResult} set of previous results
	 */
	public void execute();
	
	/**
	 * Lets {@link EvolutionFilter} filter the results and change the batch (see {@code getBatch()}).
	 * This method should be called by the {@link EvolutionExecutor}.
	 * @param results A set of results provided by the {@link EvolutionExecutor}.
	 */
	public void filter(Collection<DNAResult> results);
	
	/**
	 * @return A batch used for an execution iteration
	 */
	public HashSet<DNA> getBatch();
	
	/**
	 * This method should be called by the {@link EvolutionFilter}.
	 * Clears the current batch and adds all elements of the provided set.
	 */
	public void setBatch(Collection<DNA> batch);
	
	/**
	 * Deserializes this {@link EvolutionHandler} from Json. Include the {@link EvolutionExecutor} and {@link EvolutionFilter} aswell.
	 * @param object The Json root.
	 * @param context The serialization context.
	 */
	public void deserialize(JsonObject object, JsonDeserializationContext context) throws Exception;
	
	/**
	 * Serializes this {@link EvolutionHandler} to Json. Include the {@link EvolutionExecutor} and {@link EvolutionFilter} aswell.
	 * @param object The Json root.
	 * @param context The serialization context.
	 */
	public JsonObject serialize(JsonSerializationContext context);
	
	/**
	 * @return The {@link EvolutionExecutor}.
	 */
	public EvolutionExecutor getExecutor();
	
	/**
	 * @return The {@link EvolutionFilter}.
	 */
	public EvolutionFilter getFilter();
}
