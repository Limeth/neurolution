package cz.limeth.neurolution.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;

public class MathHelper
{
	private MathHelper() {}
	
	public static void rotate(float[] vector, float angle, int offset)
	{
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);

		float newX = vector[offset] * cos - vector[offset + 1] * sin;
		float newY = vector[offset] * sin + vector[offset + 1] * cos;
		
		vector[offset] = newX;
		vector[offset + 1] = newY;
	}
	
	public static void rotateAll(float[] vertices, float angle)
	{
		for(int i = 0; i < vertices.length - 1; i += 2)
			rotate(vertices, angle, i);
	}
	
	public static float normalizeAngle(float radians)
	{
		float normalAngle = (radians < 0 ? -radians : radians) % (MathUtils.PI * 2);
		
		if(radians < 0)
			normalAngle = MathUtils.PI * 2 - normalAngle;
		
		return normalAngle;
	}
	
	public static float getHue(Color color, Float brightness, Float saturation)
	{
		float hue;
		
		if(brightness == null)
			brightness = getBrightness(color);
		
		if(saturation == null)
			saturation = getSaturation(color, brightness);
		
		float f4 = (brightness - color.r) * (brightness - saturation);
		float f5 = (brightness - color.g) * (brightness - saturation);
		float f6 = (brightness - color.b) * (brightness - saturation);
		
		if(color.r == brightness)
			hue = f6 - f5;
		else if(color.g == brightness)
			hue = 2.0F + f4 - f6;
		else
			hue = 4.0F + f5 - f4;
		
		hue /= 6.0F;
		
		if(hue < 0.0F)
			hue += 1.0F;
		
		return hue;
	}
	
	public static float getHue(Color color, Float brightness)
	{
		return getHue(color, brightness, null);
	}
	
	public static float getHue(Color color)
	{
		return getHue(color, null, null);
	}
	
	public static float getSaturation(Color color, Float brightness)
	{
		if(brightness == null)
			brightness = getBrightness(color);
		
		float mostDarkComponent;
		
		if(color.r < color.g)
			if(color.r < color.b)
				mostDarkComponent = color.r;
			else
				mostDarkComponent = color.b;
		else
			if(color.g < color.b)
				mostDarkComponent = color.g;
			else
				mostDarkComponent = color.b;
		
		return (brightness - mostDarkComponent) * brightness;
	}
	
	public static float getSaturation(Color color)
	{
		return getSaturation(color, null);
	}
	
	public static float getBrightness(Color color)
	{
		if(color.r > color.g)
			if(color.r > color.b)
				return color.r;
			else
				return color.b;
		else
			if(color.g > color.b)
				return color.g;
			else
				return color.b;
	}
}
