package cz.limeth.neurolution.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.sun.istack.internal.Nullable;

import cz.limeth.neurolution.entities.SComponent;
import cz.limeth.neurolution.entities.SEntity;
import cz.limeth.neurolution.entities.SEntitySet;

public class SEntityRayCastCallback implements RayCastCallback
{
	private final Body excludedBody;
	private final SEntitySet entities;
	private SEntity hitEntity;
	private SComponent hitComponent;
	private Vector2 hitPoint;
	
	public SEntityRayCastCallback(@Nullable Body excludedBody, SEntitySet entities)
	{
		this.excludedBody = excludedBody;
		this.entities = entities;
	}
	
	@Override
	public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction)
	{
		Body hitBody = fixture.getBody();
		
		if(excludedBody != null && hitBody == excludedBody)
			return 1; //Continue
		
		hitEntity = entities.get(hitBody);
		
		if(hitEntity == null)
			return 1; //Continue
		
		hitComponent = hitEntity.getComponent(fixture);
		hitPoint = point;
		
		return 0.5f; //Break
	}
	
	public SEntity getHitEntity()
	{
		return hitEntity;
	}
	
	public SComponent getHitComponent()
	{
		return hitComponent;
	}
	
	public Vector2 getHitPoint()
	{
		return hitPoint;
	}
}