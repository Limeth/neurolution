package cz.limeth.neurolution.util;

import java.util.Objects;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class BodyBuilder
{
	private BodyDef bodyDef;
	private World world;
	private Array<FixtureDef> fixtureDefs;
	
	public BodyBuilder bodyDef(BodyDef bodyDef)
	{
		this.bodyDef = bodyDef;
		return this;
	}
	
	public BodyBuilder world(World world)
	{
		this.world = world;
		return this;
	}
	
	public BodyBuilder fixtureDefs(FixtureDef... fixtureDefs)
	{
		this.fixtureDefs = Array.with(fixtureDefs);
		return this;
	}
	
	public Body build()
	{
		Objects.requireNonNull(bodyDef, "The bodyDef must not be null!");
		Objects.requireNonNull(world, "The world must not be null!");
		
		Body body = world.createBody(bodyDef);
		
		if(fixtureDefs != null)
			for(FixtureDef fixtureDef : fixtureDefs)
				body.createFixture(fixtureDef);
		
		return body;
	}
}
