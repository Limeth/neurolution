package cz.limeth.neurolution.util;

import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;

public class FixtureDefBuilder implements Cloneable
{
	private final FixtureDef fixtureDef;
	
	public FixtureDefBuilder(FixtureDef fixtureDef)
	{
		this.fixtureDef = cloneFixtureDef(fixtureDef);
	}
	
	public FixtureDefBuilder()
	{
		this(new FixtureDef());
	}
	
	public FixtureDef build()
	{
		return cloneFixtureDef(fixtureDef);
	}
	
	/**
	 * @Deprecated Doesn't clone the shape
	 */
	@Deprecated
	public static FixtureDef cloneFixtureDef(FixtureDef fixtureDef)
	{
		FixtureDef result = new FixtureDef();
		
		result.density = fixtureDef.density;
		result.filter.categoryBits = fixtureDef.filter.categoryBits;
		result.filter.groupIndex = fixtureDef.filter.groupIndex;
		result.filter.maskBits = fixtureDef.filter.maskBits;
		result.friction = fixtureDef.friction;
		result.isSensor = fixtureDef.isSensor;
		result.restitution = fixtureDef.restitution;
		result.shape = fixtureDef.shape; //TODO improve
		
		return result;
	}
	
	@Override
	public FixtureDefBuilder clone()
	{
		return new FixtureDefBuilder(fixtureDef);
	}
	
	public FixtureDefBuilder density(float density)
	{
		fixtureDef.density = density;
		return this;
	}
	
	public FixtureDefBuilder filter(Filter filter)
	{
		fixtureDef.filter.categoryBits = filter.categoryBits;
		fixtureDef.filter.groupIndex = filter.groupIndex;
		fixtureDef.filter.maskBits = filter.maskBits;
		return this;
	}
	
	public FixtureDefBuilder filterCategoryBits(short filterCategoryBits)
	{
		fixtureDef.filter.categoryBits = filterCategoryBits;
		return this;
	}
	
	public FixtureDefBuilder filterGroupIndex(short filterGroupIndex)
	{
		fixtureDef.filter.groupIndex = filterGroupIndex;
		return this;
	}
	
	public FixtureDefBuilder filterMaskBits(short filterMaskBits)
	{
		fixtureDef.filter.maskBits = filterMaskBits;
		return this;
	}
	
	public FixtureDefBuilder friction(float friction)
	{
		fixtureDef.friction = friction;
		return this;
	}
	
	public FixtureDefBuilder isSensor(boolean isSensor)
	{
		fixtureDef.isSensor = isSensor;
		return this;
	}
	
	public FixtureDefBuilder restitution(float restitution)
	{
		fixtureDef.restitution = restitution;
		return this;
	}
	
	/**
	 * @Deprecated Doesn't clone the shape
	 */
	@Deprecated
	public FixtureDefBuilder shape(Shape shape)
	{
		fixtureDef.shape = shape; //TODO improve
		return this;
	}
	
	public static interface ShapeConstructor<T extends Shape>
	{
		public T construct();
	}
}
