package cz.limeth.neurolution.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class BodyDefBuilder implements Cloneable
{
	private final BodyDef bodyDef;
	
	public BodyDefBuilder(BodyDef bodyDef)
	{
		this.bodyDef = cloneBodyDef(bodyDef);
	}
	
	public BodyDefBuilder()
	{
		this(new BodyDef());
	}
	
	public BodyDef build()
	{
		return cloneBodyDef(bodyDef);
	}
	
	public static BodyDef cloneBodyDef(BodyDef bodyDef)
	{
		BodyDef result = new BodyDef();
		
		result.active = bodyDef.active;
		result.allowSleep = bodyDef.allowSleep;
		result.angle = bodyDef.angle;
		result.angularDamping = bodyDef.angularDamping;
		result.angularVelocity = bodyDef.angularVelocity;
		result.awake = bodyDef.awake;
		result.bullet = bodyDef.bullet;
		result.fixedRotation = bodyDef.fixedRotation;
		result.gravityScale = bodyDef.gravityScale;
		result.linearDamping = bodyDef.linearDamping;
		result.linearVelocity.set(bodyDef.linearVelocity);
		result.position.set(bodyDef.position);
		result.type = bodyDef.type;
		
		return result;
	}
	
	@Override
	public BodyDefBuilder clone()
	{
		return new BodyDefBuilder(bodyDef);
	}
	
	public BodyDefBuilder active(boolean active)
	{
		bodyDef.active = active;
		return this;
	}
	
	public BodyDefBuilder allowSleep(boolean allowSleep)
	{
		bodyDef.allowSleep = allowSleep;
		return this;
	}
	
	public BodyDefBuilder angle(float angle)
	{
		bodyDef.angle = angle;
		return this;
	}
	
	public BodyDefBuilder angularDamping(float angularDamping)
	{
		bodyDef.angularDamping = angularDamping;
		return this;
	}
	
	public BodyDefBuilder angularVelocity(float angularVelocity)
	{
		bodyDef.angularVelocity = angularVelocity;
		return this;
	}
	
	public BodyDefBuilder awake(boolean awake)
	{
		bodyDef.awake = awake;
		return this;
	}
	
	public BodyDefBuilder bullet(boolean bullet)
	{
		bodyDef.bullet = bullet;
		return this;
	}
	
	public BodyDefBuilder fixedRotation(boolean fixedRotation)
	{
		bodyDef.fixedRotation = fixedRotation;
		return this;
	}
	
	public BodyDefBuilder gravityScale(float gravityScale)
	{
		bodyDef.gravityScale = gravityScale;
		return this;
	}
	
	public BodyDefBuilder linearDamping(float linearDamping)
	{
		bodyDef.linearDamping = linearDamping;
		return this;
	}
	
	public BodyDefBuilder linearVelocity(Vector2 linearVelocity)
	{
		bodyDef.linearVelocity.set(linearVelocity);
		return this;
	}
	
	public BodyDefBuilder linearVelocity(float x, float y)
	{
		bodyDef.linearVelocity.set(x, y);
		return this;
	}
	
	public BodyDefBuilder position(Vector2 position)
	{
		bodyDef.position.set(position);
		return this;
	}
	
	public BodyDefBuilder position(float x, float y)
	{
		bodyDef.position.set(x, y);
		return this;
	}
	
	public BodyDefBuilder type(BodyType type)
	{
		bodyDef.type = type;
		return this;
	}
}
