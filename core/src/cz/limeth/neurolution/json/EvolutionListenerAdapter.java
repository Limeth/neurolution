package cz.limeth.neurolution.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cz.limeth.neurolution.evolution.EvolutionListener;

public class EvolutionListenerAdapter implements JsonSerializer<EvolutionListener>, JsonDeserializer<EvolutionListener>
{
	private static final EvolutionListenerAdapter instance = new EvolutionListenerAdapter();
	
	private EvolutionListenerAdapter() {}
	
	@Override
	public JsonElement serialize(EvolutionListener src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		JsonObject data = src.serialize(context);
		
		root.addProperty("type", src.getClass().getCanonicalName());
		root.add("data", data);
		
		return root;//JsonCompresser.compress(root);
	}
	
	@Override
	public EvolutionListener deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		try
		{
			JsonObject root = JsonCompresser.decompress(json).getAsJsonObject();
			JsonObject data = root.get("data").getAsJsonObject();
			String type = root.get("type").getAsString();
			Class<?> clazz = Class.forName(type);
			EvolutionListener listener = (EvolutionListener) clazz.newInstance();
			
			listener.deserialize(data, context);
			
			return listener;
		}
		catch(Exception e)
		{
			throw new JsonParseException(e);
		}
	}
	
	public static EvolutionListenerAdapter getInstance()
	{
		return instance;
	}
}
