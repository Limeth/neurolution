package cz.limeth.neurolution.json;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.zip.Adler32;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterInputStream;
import java.util.zip.Inflater;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class JsonCompresser
{
	private static final Charset CHARSET = Charset.forName("UTF-8");
	private static final Gson GSON = new GsonBuilder().create();
	private static final int BUFFER_SIZE = 1024;
	
	private JsonCompresser() {}
	
	public static JsonObject compress(JsonElement element)
	{
		JsonObject root = new JsonObject();
		
		root.addProperty("compressed", true);
		
		String inflated = GSON.toJson(element);
		byte[] inflatedBytes = inflated.getBytes(CHARSET);
		Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION);
		byte[] buffer = new byte[BUFFER_SIZE];
		String deflated = "";
		
		deflater.setInput(inflatedBytes);
		deflater.finish();
		
		while(!deflater.finished())
		{
			int amount = deflater.deflate(buffer);
			deflated += new String(buffer, 0, amount, CHARSET);
		}
		
		deflater.end();
		root.addProperty("data", deflated);
		
		return root;
	}
	
	public static JsonElement decompress(JsonElement root) throws DataFormatException
	{
		try
		{
			JsonObject rootObject = root.getAsJsonObject();
			
			if(!rootObject.has("compressed") || !rootObject.get("compressed").getAsBoolean())
				return root;
			
			String deflated = rootObject.get("data").getAsString();
			byte[] deflatedBytes = deflated.getBytes(CHARSET);
			Inflater inflater = new Inflater();
			byte[] buffer = new byte[BUFFER_SIZE];
			String inflated = "";
			
			inflater.setInput(deflatedBytes);
			
			while(!inflater.finished())
			{
				int amount = inflater.inflate(buffer);
				
				if(amount == 0 && inflater.needsDictionary())
				{
//					Adler32 dictAdler = new Adler32();
//					
//					dictAdler.update(inflater.getAdler());
//					
//					inflater.setDictionary();
//					continue;
				}
				
				inflated += new String(buffer, 0, amount, CHARSET);
			}

			inflater.end();
			
			JsonParser parser = new JsonParser();
			
			return parser.parse(inflated);
		}
		catch(Exception e)
		{
			return root;
		}
	}
	
	public static JsonElement decompress(String serialized) throws DataFormatException
	{
		JsonParser parser = new JsonParser();
		JsonElement root = parser.parse(serialized);
		
		return decompress(root);
	}
}
