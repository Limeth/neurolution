package cz.limeth.neurolution.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cz.limeth.neurolution.evolution.EvolutionHandler;

public class EvolutionHandlerAdapter implements JsonSerializer<EvolutionHandler>, JsonDeserializer<EvolutionHandler>
{
	private static final EvolutionHandlerAdapter instance = new EvolutionHandlerAdapter();
	
	private EvolutionHandlerAdapter() {}
	
	@Override
	public JsonElement serialize(EvolutionHandler src, Type typeOfSrc, JsonSerializationContext context)
	{
		JsonObject root = new JsonObject();
		JsonObject data = src.serialize(context);
		
		root.addProperty("class", src.getClass().getCanonicalName());
		root.add("data", data);
		
		return root;//JsonCompresser.compress(root);
	}
	
	@Override
	public EvolutionHandler deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		try
		{
			JsonObject root = JsonCompresser.decompress(json).getAsJsonObject();
			JsonObject data = root.get("data").getAsJsonObject();
			String type = root.get("class").getAsString();
			Class<?> clazz = Class.forName(type);
			EvolutionHandler listener = (EvolutionHandler) clazz.newInstance();
			
			listener.deserialize(data, context);
			
			return listener;
		}
		catch(Exception e)
		{
			throw new JsonParseException(e);
		}
	}
	
	public static EvolutionHandlerAdapter getInstance()
	{
		return instance;
	}
}
