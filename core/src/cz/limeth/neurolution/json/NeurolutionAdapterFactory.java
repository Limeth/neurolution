package cz.limeth.neurolution.json;

import neuroNet.limeth.json.NetworkAdapterFactory;

import com.google.gson.GsonBuilder;

import cz.limeth.neurolution.evolution.EvolutionHandler;

public class NeurolutionAdapterFactory
{
	private NeurolutionAdapterFactory() {}
	
	public static GsonBuilder applyGsonSettings(GsonBuilder builder)
	{
		return NetworkAdapterFactory.applyGsonSettings(builder)
				.registerTypeAdapter(EvolutionHandler.class, EvolutionHandlerAdapter.getInstance());
	}
	
	public static GsonBuilder createGsonBuilder()
	{
		return applyGsonSettings(new GsonBuilder());
	}
}
