package cz.limeth.neurolution;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.google.gson.Gson;

import cz.limeth.neurolution.evolution.EvolutionHandler;
import cz.limeth.neurolution.evolution.LinearEvolutionExecutor;
import cz.limeth.neurolution.evolution.LinearEvolutionFilter;
import cz.limeth.neurolution.evolution.SimpleEvolutionHandler;
import cz.limeth.neurolution.json.NeurolutionAdapterFactory;
import cz.limeth.neurolution.screen.ScreenType;
import cz.limeth.neurolution.screen.Simulation;

public class Neurolution extends Game
{
	public static final String LOG_TAG = "Neurolution";
	private static Neurolution instance;
	private static Application application;
	private final String[] args;
	
	public Neurolution(String[] args)
	{
		this.args = args;
	}
	
	@Override
	public void create()
	{
		instance = this;
		
		if(args != null && args.length > 0)
		{
			if(args[0].equalsIgnoreCase("new"))
			{
				if(args.length <= 3)
				{
					System.out.println("new <dnaPerBatch> <precision> <foodDensity> <removalFraction>");
					System.exit(0);
				}
				
				int dnaPerBatch = Integer.parseInt(args[1]);
				int precision = Integer.parseInt(args[2]);
				float foodDensity = Float.parseFloat(args[3]);
				float removalFraction = Float.parseFloat(args[4]);
				
				LinearEvolutionExecutor executor = new LinearEvolutionExecutor(precision, foodDensity);
				LinearEvolutionFilter filter = new LinearEvolutionFilter(removalFraction);
				
				setScreen(new Simulation(200, 200, new SimpleEvolutionHandler(dnaPerBatch, executor, filter), false));
				return;
			}
			else if(args[0].equalsIgnoreCase("load"))
			{
				if(args.length <= 1)
				{
					System.out.println("load <path>");
					System.exit(0);
				}
				
				String filename = args[1];
				
				for(int i = 2; i < args.length; i++)
					filename += " " + args[i];
				
				FileHandle file = Gdx.files.getFileHandle(filename, FileType.Local);
				Gson gson = NeurolutionAdapterFactory.createGsonBuilder().create();
				EvolutionHandler handler = gson.fromJson(file.reader(), EvolutionHandler.class);
				
				setScreen(new Simulation(200, 200, handler, false));
				return;
			}
			else
			{
				System.out.println("new - Creates a new simulation");
				System.out.println("load [File] - Loads a saved simulation");
				System.exit(0);
			}
		}
		
		setScreen(ScreenType.MAIN_MENU);
	}
	
	public boolean setScreen(ScreenType type)
	{
		try
		{
			setScreen(type.newInstance());
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static void removeInputProcessor()
	{
		Gdx.input.setInputProcessor(null);
	}
	
	public static Neurolution getInstance()
	{
		return instance;
	}
	
	public static Application getApplication()
	{
		return application;
	}
	
	public static void setApplication(Application application)
	{
		Neurolution.application = application;
	}
}
