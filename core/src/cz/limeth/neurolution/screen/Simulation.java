package cz.limeth.neurolution.screen;

import java.util.ArrayList;
import java.util.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cz.limeth.neurolution.NConst;
import cz.limeth.neurolution.entities.EntityFood;
import cz.limeth.neurolution.entities.EntityWall;
import cz.limeth.neurolution.entities.SComponent;
import cz.limeth.neurolution.entities.SEntity;
import cz.limeth.neurolution.entities.SEntitySet;
import cz.limeth.neurolution.entities.creature.EntityCreature;
import cz.limeth.neurolution.evolution.EvolutionHandler;

public class Simulation implements Screen, InputProcessor, ContactListener
{
	private static final Vector3 $vector3 = new Vector3();
	private static Simulation instance;
	private EvolutionHandler evolutionHandler;
	private World world;
	private OrthographicCamera camera;
	private Viewport viewport;
	private Box2DDebugRenderer debugRenderer;
	private ShapeRenderer renderer;
	private SpriteBatch spriteBatch;
	private EntityWall wall;
	private float width, height;
	private final SEntitySet entities = new SEntitySet();
	private boolean followSelectedEntity, fastForward, debug, hidden, graphics;
	private SEntity selectedEntity;
	private int fastSpeed = 5;
	private Long lastFPSMeasurement, lastFrame;
	private Thread asyncGameLoopThread;
	private final Object lock = new Object();
	private ArrayList<Runnable> runLater = new ArrayList<Runnable>();
	private ArrayList<Runnable> $runLaterTemp = new ArrayList<Runnable>();

	public Simulation(float width, float height, EvolutionHandler evolutionHandler, boolean graphics)
	{
		instance = this;
		this.width = width;
		this.height = height;
		camera = new OrthographicCamera(width, height);
		camera.position.set(width / 2, height / 2, 0);
		camera.update();
		viewport = new ExtendViewport(width, height, camera);
		this.evolutionHandler = evolutionHandler;

		if(!(this.graphics = graphics))
		{
			hidden = true;
			fastForward = true;
		}
	}

	@Override
	public void show()
	{
		world = new World(new Vector2(0, 0), true);

		world.setContactListener(this);
		Gdx.input.setInputProcessor(this);

		wall = new EntityWall(world, new Vector2(width / 2, height / 2), new Vector2(width, height));
		entities.add(wall);

		evolutionHandler.execute();
		renderer = new ShapeRenderer();
		spriteBatch = new SpriteBatch();
		debugRenderer = new Box2DDebugRenderer();
		
		if(!graphics)
			toggleAsyncGameLoopThread();
	}

	@Override
	public void render(float delta)
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if(selectedEntity != null)
		{
			Body body = selectedEntity.getBody();

			if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
			{
				body.setAngularVelocity(0);
				body.setLinearVelocity(0, 0);
				body.setAwake(false);
			}

			if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
				if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
					body.setAngularVelocity(1);
				else
					body.applyForceToCenter(-10000f, 0, true);

			if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
				if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
					body.setAngularVelocity(-1);
				else
					body.applyForceToCenter(10000f, 0, true);

			if(Gdx.input.isKeyPressed(Input.Keys.UP))
				body.applyForceToCenter(0, 10000f, true);

			if(Gdx.input.isKeyPressed(Input.Keys.DOWN))
				body.applyForceToCenter(0, -10000f, true);
		}

		if(asyncGameLoopThread == null)
		{
			if(fastForward)
			{
				float fps;
				long now = TimeUtils.millis();

				if(lastFrame != null)
				{
					long delayMillis = now - lastFrame;
					float delayFraction = delayMillis / 16f;
					fps = 60f / delayFraction;
				}
				else
					fps = 60;

				lastFrame = now;

				if(lastFPSMeasurement == null || lastFPSMeasurement + 100 < now)
				{
					if(fps < 60)
					{
						fastSpeed = (int) Math.ceil(fastSpeed * fps / 60);

						if(fastSpeed < 1)
							fastSpeed = 1;
					}
					else
						fastSpeed++;

					lastFPSMeasurement = now;
				}

				for(int i = 0; i < fastSpeed; i++)
				{
					world.step(1 / 60f, 8, 3);
					entities.stepAll(1);
				}
			}
			else
			{
				world.step(1 / 60f, 8, 3);
				entities.stepAll(1);
			}

			executePostRunnables();
		}

		if(hidden)
			return;

		if(followSelectedEntity && selectedEntity != null)
		{
			Body body = selectedEntity.getBody();
			Vector2 position = body.getPosition();

			camera.position.set(position, 0);
			camera.update();
		}

		synchronized(lock)
		{
			renderer.setProjectionMatrix(camera.combined);
			spriteBatch.setProjectionMatrix(camera.combined);
			// renderer.setAutoShapeType(true);
			renderer.begin(ShapeType.Filled);
			spriteBatch.begin();
			entities.renderAll(renderer, spriteBatch, camera.combined, delta);
			renderer.end();
			spriteBatch.end();

			if(debug)
				debugRenderer.render(world, camera.combined);
		}
	}

	@Override
	public void beginContact(Contact contact)
	{
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		Body bodyA = fixtureA.getBody();
		Body bodyB = fixtureB.getBody();
		SEntity entityA = entities.get(bodyA);
		SEntity entityB = entities.get(bodyB);
		final EntityFood food;
		EntityCreature creature;

		if(entityA instanceof EntityFood)
			food = (EntityFood) entityA;
		else if(entityB instanceof EntityFood)
			food = (EntityFood) entityB;
		else
			return;

		if(entityA instanceof EntityCreature)
			creature = (EntityCreature) entityA;
		else if(entityB instanceof EntityCreature)
			creature = (EntityCreature) entityB;
		else
			return;

		if(creature.isDead() || !creature.isSpawned() || food.isDead() || !food.isSpawned())
			return;

		food.decrease(1);
		creature.increaseEnergy(NConst.CREATURE_MAX_ENERGY_MODIFIER / 2);

		if(food.isEaten())
			food.postDespawn();
	}

	@Override
	public void endContact(Contact contact)
	{
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold)
	{
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse)
	{
	}

	@Override
	public void resize(int width, int height)
	{
		viewport.update(width, height);
	}

	@Override
	public void hide()
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}

	@Override
	public void dispose()
	{
		world.dispose();
		renderer.dispose();
		spriteBatch.dispose();
		debugRenderer.dispose();
	}

	@Override
	public boolean keyDown(int keycode)
	{
		if(keycode == Keys.D)
		{
			debug = !debug;
			return true;
		}
		else if(keycode == Keys.S)
		{
			fastForward = !fastForward;
			return true;
		}
		else if(keycode == Keys.F)
		{
			followSelectedEntity = !followSelectedEntity;
			return true;
		}
		else if(keycode == Keys.A && fastForward)
		{
			toggleAsyncGameLoopThread();
			return true;
		}
		else if(keycode == Keys.H)
		{
			hidden = !hidden;
			return true;
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		if(Gdx.input.isButtonPressed(Buttons.LEFT))
		{
			Array<Body> bodies = Array.of(Body.class);

			$vector3.set(screenX, screenY, 0);
			camera.unproject($vector3);
			world.getBodies(bodies);

			if(!Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) && !Gdx.input.isKeyPressed(Keys.CONTROL_RIGHT))
				selectedEntity = null;

			outerLoop: for(SEntity entity : entities)
				for(SComponent component : entity.getComponents())
				{
					if(component == null)
						continue;

					Fixture fixture = component.getFixture();

					if(fixture.testPoint($vector3.x, $vector3.y))
					{
						selectedEntity = entity;
						break outerLoop;
					}
				}
		}

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		if(Gdx.input.isButtonPressed(Buttons.RIGHT))
		{
			float dX = Gdx.input.getDeltaX();
			float dY = Gdx.input.getDeltaY();

			camera.translate(-dX * 2 * camera.zoom, dY * 2 * camera.zoom);
			camera.update();
			return true;
		}

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		camera.zoom *= Math.pow(2, amount);
		camera.update();
		return true;
	}

	public boolean toggleAsyncGameLoopThread()
	{
		if(asyncGameLoopThread == null)
		{
			startAsyncGameLoopThread();
			return true;
		}
		else
		{
			stopAsyncGameLoopThread();
			return false;
		}
	}

	public Thread startAsyncGameLoopThread()
	{
		asyncGameLoopThread = new Thread(GAME_STEP_RUNNABLE);

		asyncGameLoopThread.start();

		return asyncGameLoopThread;
	}

	public void stopAsyncGameLoopThread()
	{
		if(asyncGameLoopThread != null)
		{
			Thread thread = asyncGameLoopThread;
			asyncGameLoopThread = null;

			while(thread.isAlive())
			{
				try
				{
					Thread.sleep(10);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public SEntity getSelectedEntity()
	{
		return selectedEntity;
	}

	public boolean isSelected(SEntity entity)
	{
		return entity == selectedEntity;
	}

	public void deselect()
	{
		selectedEntity = null;
	}

	public SEntitySet getEntities()
	{
		return entities;
	}

	public boolean isDebug()
	{
		return debug;
	}

	public static Simulation getInstance()
	{
		return instance;
	}

	public World getWorld()
	{
		return world;
	}

	public float getWidth()
	{
		return width;
	}

	public float getHeight()
	{
		return height;
	}

	public Object getLock()
	{
		return lock;
	}

	public void runLater(Runnable runnable)
	{
		Objects.requireNonNull(runnable);

		synchronized(lock)
		{
			runLater.add(runnable);
		}
	}

	private void executePostRunnables()
	{
		synchronized(lock)
		{
			$runLaterTemp.clear();
			$runLaterTemp.addAll(runLater);

			for(Runnable runnable : $runLaterTemp)
				runnable.run();

			runLater.removeAll($runLaterTemp);
		}
	}

	private final Runnable GAME_STEP_RUNNABLE = new Runnable()
	{
		@Override
		public void run()
		{
			while(Thread.currentThread() == asyncGameLoopThread)
			{
				synchronized(lock)
				{
					world.step((1f / fastSpeed) / 60f, 8, 3);
					entities.stepAll(1f / fastSpeed);
					executePostRunnables();
				}
			}
		}
	};
}
