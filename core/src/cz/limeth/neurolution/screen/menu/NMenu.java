package cz.limeth.neurolution.screen.menu;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import cz.limeth.neurolution.NConst;

public abstract class NMenu implements Screen
{
	static
	{
		SpriteBatch $batch = new SpriteBatch();
		TextureAtlas $buttonAtlas = new TextureAtlas(Gdx.files.internal("atlas/button.pack"));
		Skin $buttonSkin = new Skin($buttonAtlas);
		BitmapFont $font = new BitmapFont(Gdx.files.internal("font/openSans.fnt"));
		TextButtonStyle $buttonStyle = new TextButtonStyle();
		$buttonStyle.fontColor = Color.RED;
		$buttonStyle.up = $buttonSkin.getDrawable("button");
		$buttonStyle.down = $buttonSkin.getDrawable("buttonPressed");
		$buttonStyle.font = $font;
		
		FONT = $font;
		BUTTON_ATLAS = $buttonAtlas;
		BUTTON_SKIN = $buttonSkin;
		SPRITE_BATCH = $batch;
		BUTTON_STYLE = $buttonStyle;
	}

	public static final BitmapFont FONT;
	public static final TextureAtlas BUTTON_ATLAS;
	public static final Skin BUTTON_SKIN;
	public static final SpriteBatch SPRITE_BATCH;
	public static final TextButtonStyle BUTTON_STYLE;
	private ScreenViewport viewport;
	private Stage stage;
	private ArrayList<Button> buttons = new ArrayList<Button>();
	
	public NMenu(TextButton... buttons)
	{
		viewport = new ScreenViewport();
		stage = new Stage(viewport);
		
		for(TextButton button : buttons)
			addButton(button);
	}
	
	public Button addButton(String text, InputListener listener)
	{
		TextButton button = new TextButton(text, BUTTON_STYLE);
		
		button.setColor(1, 1, 1, 1);
		button.setSize(NConst.MAIN_MENU_BUTTON_WIDTH, NConst.MAIN_MENU_BUTTON_HEIGHT);
		button.addListener(listener);
		
		addButton(button);
		
		return button;
	}
	
	public void addButton(Button button)
	{
		buttons.add(button);
		stage.addActor(button);
	}
	
	public boolean removeButton(Button button)
	{
		return buttons.remove(button) && button.remove();
	}
	
	public Button[] getButtons()
	{
		return buttons.toArray(new Button[buttons.size()]);
	}
	
	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void render(float delta)
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(delta);
		SPRITE_BATCH.begin();
		stage.draw();
		SPRITE_BATCH.end();
	}

	@Override
	public void resize(int width, int height)
	{
		viewport.update(width, height, true);
		
		float menuHeight = buttons.size() * NConst.MAIN_MENU_BUTTON_HEIGHT + (buttons.size() - 1) * NConst.MAIN_MENU_BUTTON_SPACING;
		
		for(int i = 0; i < buttons.size(); i++)
		{
			Button button = buttons.get(i);
			
			if(button == null)
				continue;
			
			float x = (width - NConst.MAIN_MENU_BUTTON_WIDTH) / 2f;
			float y = ((height + menuHeight) / 2f) - i * (NConst.MAIN_MENU_BUTTON_HEIGHT + NConst.MAIN_MENU_BUTTON_SPACING);
			
			button.setPosition(x, y);
		}
	}

	@Override
	public void dispose()
	{
		stage.dispose();
	}
}
