package cz.limeth.neurolution.screen.menu;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.google.gson.Gson;

import cz.limeth.neurolution.NConst;
import cz.limeth.neurolution.Neurolution;
import cz.limeth.neurolution.evolution.EvolutionHandler;
import cz.limeth.neurolution.json.NeurolutionAdapterFactory;
import cz.limeth.neurolution.listeners.ButtonListener;
import cz.limeth.neurolution.screen.Simulation;

public class LoadMenu extends NMenu
{
	public LoadMenu()
	{
		FileHandle file = Gdx.files.getFileHandle(NConst.PATH_SAVES, FileType.Local);
		
		for(FileHandle saveFile : file.list())
		{
			String name = saveFile.nameWithoutExtension();
			
			addButton(name, new LoadButtonListener(saveFile));
		}
	}

	@Override
	public void hide()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}
	
	private static class LoadButtonListener extends ButtonListener
	{
		private final FileHandle file;
		
		public LoadButtonListener(FileHandle file)
		{
			this.file = file;
		}
		
		@Override
		public void press(InputEvent event, float x, float y, int pointer, int button)
		{
			EvolutionHandler evolutionHandler = loadEvolutionHandler();
			Simulation simulation = new Simulation(200, 200, evolutionHandler, true);
			
			Neurolution.removeInputProcessor();
			Neurolution.getInstance().setScreen(simulation);
		}
		
		public EvolutionHandler loadEvolutionHandler()
		{
			Gson gson = NeurolutionAdapterFactory.createGsonBuilder().create();
			
			return gson.fromJson(file.reader(), EvolutionHandler.class);
		}
		
		public FileHandle getFile()
		{
			return file;
		}
	}
}
