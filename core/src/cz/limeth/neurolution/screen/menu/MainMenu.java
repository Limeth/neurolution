package cz.limeth.neurolution.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import cz.limeth.neurolution.Neurolution;
import cz.limeth.neurolution.evolution.SimpleEvolutionHandler;
import cz.limeth.neurolution.listeners.ButtonListener;
import cz.limeth.neurolution.screen.Simulation;

public class MainMenu extends NMenu
{
	public MainMenu()
	{
		super();
		
		addButton("New Neurolution", new ButtonListener()
			{
				@Override
				public void press(InputEvent event, float x, float y, int pointer, int button)
				{
					Neurolution.removeInputProcessor();
					Neurolution.getInstance().setScreen(new Simulation(200, 200, SimpleEvolutionHandler.newDefault(), true));
				}
			}
		);
		
		addButton("Load Neurolution", new ButtonListener()
			{
				@Override
				public void press(InputEvent event, float x, float y, int pointer, int button)
				{
					Neurolution.removeInputProcessor();
					Neurolution.getInstance().setScreen(new LoadMenu());
				}
			}
		);
		
		addButton("Quit", new ButtonListener()
			{
				@Override
				public void press(InputEvent event, float x, float y, int pointer, int button)
				{
					Gdx.app.exit();
				}
			}
		);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
}
