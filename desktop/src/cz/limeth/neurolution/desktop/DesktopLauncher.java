package cz.limeth.neurolution.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import cz.limeth.neurolution.Neurolution;

public class DesktopLauncher
{
	public static void main(String[] args)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.foregroundFPS = 0;
		config.backgroundFPS = 0;
		
		Neurolution.setApplication(new LwjglApplication(new Neurolution(args), config));
	}
}
